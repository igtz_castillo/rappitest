//
//  ErrorType.swift
//  RappiTest
//
//  Created by Israel Gtz on 8/8/19.
//  Copyright © 2019 Rohkeus Systems. All rights reserved.
//

import Foundation

enum ErrorType {
    
    enum RequestServer: Error {
        case noNetworkDetected
        case errorGettingPopularMovies
        case errorGettingTopRatedMovies
        case errorGettingUpcomingMovies
        case errorGettingSearchingMovies
        case errorGettingPopularTVShows
        case errorGettingTopRatedTVShows
        case errorGettingUpcomingTVShows
        case errorGettingSearchingTVShows
        
        case errorGettingVideosOfMovie
        case errorGettingVideosOfTVShow
        
        case noValidAnswerFromServer
        case noValidDataFromServer
        case errorFromServer
        case unknownErrorFromServer
        
        case wrongParametersInsideApp
    }
    
    //Core Dara errors
    enum LocalStorage: Error {
        case errorDecodingToCoreData
        case errorDeletingMovies
        case errorDeletingTVShows
        //
        case errorFetchingPopularMoviesFromCoreData
        case errorFetchingTopRatedMoviesFromCoreData
        case errorFetchingUpcomingMoviesFromCoreData
        case errorFetchingPopularTVShowsFromCoreData
        case errorFetchingTopRatedTVShowsFromCoreData
        case errorFetchingUpcomingTVShowsFromCoreData
        
        //I let these two 'case' but we are not going using them because we are not going to save the searched movies and TVShows into core data
        case errorFetchingSearchingMoviesFromCoreData
        case errorFetchingSearchingTVShowsFromCoreData
        
        
    }
    
    enum DownloadingImages: String, Error {
        case cancelled
        case timedOut =          "The request timed out."
        case offlineConnection = "The Internet connection appears to be offline."
        case invalidURL =        "Invalid URL."
    }
    
}
