//
//  Enums.swift
//  RappiTest
//
//  Created by Israel Gtz on 8/10/19.
//  Copyright © 2019 Rohkeus Systems. All rights reserved.
//

import Foundation

enum TypeOfMovies: Int {
    case popular = 1
    case topRated = 2
    case upcoming = 3
    case searched = 4
}

enum TypeOfTVShows: Int {
    case popular = 1
    case topRated = 2
    //case upcoming = 3  //I let this for future use
    case searched = 4
}

enum NameOfMoviesScreens: String {
    case popularMovies = "Popular Movies"
    case topRatedMovies = "Top Rated Movies"
    case upcomingMovies = "Upcoming Movies"
    case searchingMovies = "Searching Movies..."
}

enum NameOfTVShowsScreens: String {
    case popularTVShows = "Popular TV Shows"
    case topRatedTVShows = "Top Rated TV Shows"
    case upcomingTVShows = "Upcoming TV Shows"
    case searchingTVShows = "Searching TV Shows..."
}

enum Messages: String {
    
    case errorParsingData = "Sorry, I had problems getting data from internet ☹︎ \nPlease try again in some time"
    case noLocalStorageData = "Sorry, I had some problems finding information in local storage ☹︎ \nPlease try again when you have network connection"
    
    case fetchingPopularMovies = "Please wait... I'm bringing the popular movies ☺︎"
    case fetchingTopRatedMovies = "Please wait... I'm bringing the top rated movies ☺︎"
    case fetchingUpcomingMovies = "Please wait... I'm bringing the upcoming movies ☺︎"
    case fetchingSearchedMovies = "Please wait... I'm bringing the movies that match your search ☺︎"
    case fetchingPopularTVShows = "Please wait... I'm bringing the popular TV shows ☺︎"
    case fetchingTopRateTVShows = "Please wait... I'm bringing the top rated TV shows ☺︎"
    case fetchingUpcomingTVShows = "Please wait... I'm bringing the upcoming TV shows ☺︎"
    case fetchingSearchedTVShows = "Please wait... I'm bringing the TV shows that match your search ☺︎"
    
    case errorFetchingSearchedMoviesOrTVShows = "Sorry, I had problems finding information from internet ☹︎ \nPlease try again in some time"
    
    
    case errorNoNetworkDetected = "Sorry, I don't detect network connection \n Please try again when you have network connection"
    
}
