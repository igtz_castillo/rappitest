//
//  InterfaceUtil.swift
//  RappiTest
//
//  Created by Israel Gtz on 8/14/19.
//  Copyright © 2019 Rohkeus Systems. All rights reserved.
//

import Foundation
import UIKit

class InterfaceUtil: NSObject {
    
    static let shared = InterfaceUtil()
    
    var viewOfLoader: UIView! = nil
    var loader: UIActivityIndicatorView! = nil
    var messageLabel: UILabel! = UILabel.init()
    private var viewLoaderStack: Int = 0
    
    override private init(){}
    
    func currentViewController () -> UIWindow {
        var currentWindow: UIWindow = UIWindow.init()
        if UIApplication.shared.keyWindow != nil {
            currentWindow = UIApplication.shared.keyWindow!
        }
        return currentWindow
    }
    
    func showLoader(_ message: String? = nil) {
        if self.viewLoaderStack < 0 {
            self.viewLoaderStack = 0
        }
        self.viewLoaderStack = self.viewLoaderStack + 1
        let currentViewController = self.currentViewController()
        if loader != nil {
            viewOfLoader.removeConstraints(loader.constraints)
            loader.removeFromSuperview()
            loader = nil
        }
        if viewOfLoader != nil {
            currentViewController.removeConstraints(viewOfLoader.constraints)
            viewOfLoader.removeFromSuperview()
            viewOfLoader = nil
        }
        if viewOfLoader == nil {
            viewOfLoader = UIView(frame: CGRect(x: 0, y: 0, width: 0, height: 0))
            viewOfLoader.backgroundColor = UIColor.init(white: 0.0, alpha: 0.5)
        }
        currentViewController.addSubview(viewOfLoader)
        viewOfLoader.translatesAutoresizingMaskIntoConstraints = false
        let topConstraint = viewOfLoader.topAnchor.constraint(equalTo: currentViewController.topAnchor, constant: 0)
        let leadingConstraint = viewOfLoader.leadingAnchor.constraint(equalTo: currentViewController.leadingAnchor, constant: 0)
        let trailingConstraint = viewOfLoader.trailingAnchor.constraint(equalTo: currentViewController.trailingAnchor, constant: 0)
        let bottomConstraint = viewOfLoader.bottomAnchor.constraint(equalTo: currentViewController.bottomAnchor, constant: 0)
        topConstraint.isActive = true
        leadingConstraint.isActive = true
        trailingConstraint.isActive = true
        bottomConstraint.isActive = true
        currentViewController.addConstraints([topConstraint, leadingConstraint, trailingConstraint, bottomConstraint])
        
        if message == nil {
            self.createActivityIndicator(self.viewOfLoader, positionInCenter: true)
        } else {
            let centerView = UIView(frame: CGRect.init(x: 0.0, y: 0.0, width: 0.0, height: 250.0))
            centerView.backgroundColor = .clear
            viewOfLoader.addSubview(centerView)
            centerView.translatesAutoresizingMaskIntoConstraints = false
            let leftConstraint = centerView.leftAnchor.constraint(equalTo: self.viewOfLoader.leftAnchor)
            let rightConstraint = centerView.rightAnchor.constraint(equalTo: self.viewOfLoader.rightAnchor)
            let centerXConstraint = centerView.centerXAnchor.constraint(equalTo: self.viewOfLoader.centerXAnchor)
            let centerYConstraint = centerView.centerYAnchor.constraint(equalTo: self.viewOfLoader.centerYAnchor)
            leftConstraint.isActive = true
            rightConstraint.isActive = true
            centerXConstraint.isActive = true
            centerYConstraint.isActive = true
            self.viewOfLoader.addConstraints([leftConstraint, rightConstraint, centerXConstraint, centerYConstraint])
            self.createActivityIndicator(centerView, positionInCenter: false, message: message)
        }
    }
    
    private func createActivityIndicator(_ viewWhereIsGoingToBeAdded: UIView, positionInCenter: Bool, message: String? = nil) {
        if loader == nil {
            loader = UIActivityIndicatorView.init(frame: CGRect.init(x: 0.0, y: 0.0, width: 50.0, height: 50.0))
            viewWhereIsGoingToBeAdded.addSubview(loader)
            loader.translatesAutoresizingMaskIntoConstraints = false
            if positionInCenter {
                let xPositionConstraint = loader.centerXAnchor.constraint(equalTo: viewWhereIsGoingToBeAdded.centerXAnchor, constant: 0)
                let yPositionConstraint = loader.centerYAnchor.constraint(equalTo: viewWhereIsGoingToBeAdded.centerYAnchor, constant: 0)
                xPositionConstraint.isActive = true
                yPositionConstraint.isActive = true
                viewWhereIsGoingToBeAdded.addConstraints([xPositionConstraint, yPositionConstraint])
            } else {
                let xPositionCenterConstraint = loader.centerXAnchor.constraint(equalTo: viewWhereIsGoingToBeAdded.centerXAnchor)
                let heightLoaderConstraint = loader.heightAnchor.constraint(equalToConstant: 50.0)
                let widthLoaderConstraint = loader.widthAnchor.constraint(equalToConstant: 50.0)
                let topPositionConstraint = loader.topAnchor.constraint(equalTo: viewWhereIsGoingToBeAdded.topAnchor, constant: 25.0)
                xPositionCenterConstraint.isActive = true
                heightLoaderConstraint.isActive = true
                widthLoaderConstraint.isActive = true
                topPositionConstraint.isActive = true
                viewWhereIsGoingToBeAdded.addConstraints([xPositionCenterConstraint, heightLoaderConstraint, widthLoaderConstraint, topPositionConstraint])
                
                if self.messageLabel != nil {
                    self.messageLabel.removeFromSuperview()
                    self.messageLabel = nil
                }
                self.messageLabel = UILabel(frame: CGRect())
                self.messageLabel.textColor = UIColor.white
                self.messageLabel.textAlignment = .center;
                self.messageLabel.font.withSize(12.0)
                self.messageLabel.text = message
                self.messageLabel.clipsToBounds = true
                self.messageLabel.numberOfLines = 0
                viewWhereIsGoingToBeAdded.addSubview(self.messageLabel)
                self.messageLabel.translatesAutoresizingMaskIntoConstraints = false
                let topConstraint = self.messageLabel.topAnchor.constraint(equalTo: self.loader.bottomAnchor, constant: 0.0)
                let leftConstraint = self.messageLabel.leftAnchor.constraint(equalTo: viewWhereIsGoingToBeAdded.leftAnchor)
                let rightConstraint = self.messageLabel.rightAnchor.constraint(equalTo: viewWhereIsGoingToBeAdded.rightAnchor)
                let bottomConstraint = self.messageLabel.bottomAnchor.constraint(equalTo: viewWhereIsGoingToBeAdded.bottomAnchor)
                topConstraint.isActive = true
                leftConstraint.isActive = true
                rightConstraint.isActive = true
                bottomConstraint.isActive = true
                viewWhereIsGoingToBeAdded.addConstraints([topConstraint, leftConstraint, rightConstraint, bottomConstraint])
            }
        }
        loader.startAnimating()
    }
    
    func hideLoader() {
        self.viewLoaderStack = self.viewLoaderStack - 1
        if self.viewLoaderStack == 0 {
            viewOfLoader.removeFromSuperview()
            loader.stopAnimating()
        }
    }
    
    func showMessage(_ title: String?, message: String?, overViewcontroller: UIViewController? = nil, actionsToDoAfterTapOk: (() -> Void)? = nil) {
        if overViewcontroller == nil {
            let alert = UIAlertController.init(title: title, message: message, preferredStyle: .alert)
            let alertaction = UIAlertAction.init(title: "Ok", style: .default) { (alertAction) in
                actionsToDoAfterTapOk?()
            }
            alert.addAction(alertaction)
            self.currentViewController().rootViewController?.present(alert, animated: true, completion: nil)
        } else {
            let alert = UIAlertController.init(title: title, message: message, preferredStyle: .alert)
            let alertaction = UIAlertAction.init(title: "Ok", style: .default) { (alertAction) in
                actionsToDoAfterTapOk?()
            }
            alert.addAction(alertaction)
            overViewcontroller!.present(alert, animated: true, completion: nil)
        }
    }
    
}

