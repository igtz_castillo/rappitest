//
//  Models.swift
//  RappiTest
//
//  Created by Israel Gtz on 8/9/19.
//  Copyright © 2019 Rohkeus Systems. All rights reserved.
//

import Foundation

struct ConfigurationRequestServerResponse: Codable {
    var images: ConfigurationImagesModel?
    var change_keys: [String]?
}

struct ConfigurationImagesModel: Codable {
    var base_url: String?
    var secure_base_url: String?
    var backdrop_sizes: [String]?
    var logo_sizes: [String]?
    var poster_sizes: [String]?
    var profile_sizes: [String]?
    var still_sizes: [String]?
}


//Even the responses are the same I decided to create one model for each one so if the response change you just have to change the required response
protocol BasicResponseForMovies {
    var results: [Movie]? {get set}
}

struct PopularMoviesResponse: Codable, BasicResponseForMovies {
    var page: Int?
    var results: [Movie]?
    var total_results: Int?
    var total_pages: Int?
}

struct TopRatedMoviesResponse: Codable, BasicResponseForMovies {
    var page: Int?
    var results: [Movie]?
    var total_results: Int?
    var total_pages: Int?
}

struct UpcomingMoviesResponse: Codable, BasicResponseForMovies {
    var page: Int?
    var results: [Movie]?
    var total_results: Int?
    var total_pages: Int?
    var dates: Dates?
}

struct SearchingMoviesResponse: Codable, BasicResponseForMovies {
    var page: Int?
    var results: [Movie]?
    var total_results: Int?
    var total_pages: Int?
    var dates: Dates?
}

protocol BasicResponseForTVShows {
    var results: [TVShow]? {get set}
}

struct PopularTVShowResponse: Codable, BasicResponseForTVShows {
    var page: Int?
    var results: [TVShow]?
    var total_results: Int?
    var total_pages: Int?
}

struct TopRatedTVShowResponse: Codable, BasicResponseForTVShows {
    var page: Int?
    var results: [TVShow]?
    var total_results: Int?
    var total_pages: Int?
}

struct SearchingTVShowsResponse : Codable, BasicResponseForTVShows {
    var page: Int?
    var results: [TVShow]?
    var total_results: Int?
    var total_pages: Int?
}

struct MovieAndTVShowsVideosResponse: Codable {
    var id: Int?
    var results: [VideoForItem]?
}

struct VideoForItem: Codable {
    var id: String?
    var iso_639_1: String?
    var iso_3166_1: String?
    var key: String?
    var name: String?
    var site: String?
    var size: Int?
    var type: String?
}

