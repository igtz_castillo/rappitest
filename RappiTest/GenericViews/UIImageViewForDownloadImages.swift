//
//  UIImageViewForDownloadImages.swift
//  RappiTest
//
//  Created by Israel Gtz on 8/11/19.
//  Copyright © 2019 Rohkeus Systems. All rights reserved.
//

import Foundation
import UIKit

let imageCache = NSCache<NSString, UIImage>()

class UIImageViewForDownloadImages: UIImageView {
    
    var downloadImageTask: URLSessionDataTask?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    override init(image: UIImage?) {
        super.init(image: image)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    func downloadAndCacheImage(_ stringURL: String, contentMode mode: UIView.ContentMode = .scaleAspectFill, sizeToCrop: CGSize? = nil,  actionsToDoWhenError: (()->Void)? = nil, actionsWhenSucceed: (()->Void)? = nil, actionsToDoWhenCancelled: (()->Void)? = nil) {
        self.downloadImageTask?.cancel()
        self.image = nil
        self.contentMode = mode
        self.clipsToBounds = true
        if var imageCache = imageCache.object(forKey: stringURL as NSString) {
            if sizeToCrop != nil {
                imageCache = imageCache.crop(to: sizeToCrop!)
            }
            self.image = imageCache
            return
        } else {
            let url = URL.init(string: stringURL)
            if let url = url {
                self.downloadImageTask = URLSession.shared.dataTask(with: url) { data, response, error in
                    if error != nil {
                        switch error?.localizedDescription ?? "" {
                        case ErrorType.DownloadingImages.cancelled.rawValue:
                            actionsToDoWhenCancelled?()
                        case ErrorType.DownloadingImages.timedOut.rawValue, ErrorType.DownloadingImages.offlineConnection.rawValue:
                            actionsToDoWhenError?()
                        default:
                            actionsToDoWhenCancelled?()
                        }
                    } else
                        if let imageData = data {
                            var imageToCache = UIImage(data: imageData)
                            imageCache.setObject(imageToCache!, forKey: stringURL as NSString)
                            actionsWhenSucceed?()
                            DispatchQueue.main.async {
                                if sizeToCrop != nil {
                                    imageToCache = imageToCache?.crop(to: sizeToCrop!)
                                }
                                self.image = imageToCache!
                            }
                    }
                }
                self.downloadImageTask?.resume()
            }
            actionsToDoWhenError?()
        }
    }
    
}
