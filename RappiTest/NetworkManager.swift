//
//  NetworkManager.swift
//  RappiTest
//
//  Created by Israel Gtz on 8/8/19.
//  Copyright © 2019 Rohkeus Systems. All rights reserved.
//

import Foundation
import Alamofire
import PromiseKit
import CoreData

enum RequestType: String {
    case getPopularMovies
    case getTopRatedMovies
    case getUpcomingMovies
    case getSearchingMovies
    case getPopularTVShows
    case getTopRatedTVShows
    case getUpcomingTVShows
    case getSearchingTVShows
    
    //
    case getVideosOfMovie
    case getVideosOfTVShow
    
    //configuration - this is required for getting paths for accessing multimedia files
    case getConfigurationPaths

}

enum RequestUrl: String {
    case getPopularMoviesURL = "/movie/popular"
    case getTopRatedMoviesURL = "/movie/top_rated"
    case getUpcomingMoviesURL = "/movie/upcoming"
    case getSearchingMoviesURL = "/search/movie"
    case getPopularTVShowsURL = "/tv/popular"
    case getTopRatedTVShowsURL = "/tv/top_rated"
    case getUpcomingTVShowsURL = ""
    case getSearchingTVShowsURL = "/search/tv"
    
    //
    case getVideosOfMovieURL = "/movie/{movie_id}/videos"
    case getVideosOfTVShowURL = "/tv/{tv_id}/videos"
    
    //configuration - this is required for getting paths for accessing multimedia files
    case getConfigurationPathsURL = "/configuration"
}

class NetworkManager: NSObject {
    
    static let shared = NetworkManager()
    private let baseURLServer = "https://api.themoviedb.org/3"
    private let apiKey = "5b3a4ac9aee9d07e9f153eba21579ed9"
    private var apiKeyVariableForRequest: String {
        get {
            return "?api_key=" + self.apiKey
        }
    }
    private var configurationResponse: ConfigurationRequestServerResponse?
    
    override private init(){}
    
    public func doRequest(type: RequestType, parameters: [String: Any]?) -> Promise<Any> {
        return Promise { result in
            switch type {
            case .getPopularMovies:
                self.requestForGetPopularMovies(parameters: parameters).done({ (responseFromServer) in
                    guard let response = responseFromServer as? PopularMoviesResponse else {
                        return result.reject(ErrorType.RequestServer.errorGettingPopularMovies)
                    }
                    return result.fulfill(response)
                }).catch({ (error) in
                    return result.reject(error)
                })
            case .getTopRatedMovies:
                self.requestForGetTopRatedMovies(parameters: parameters).done({ (responseFromServer) in
                    guard let response = responseFromServer as? TopRatedMoviesResponse else {
                        return result.reject(ErrorType.RequestServer.errorGettingTopRatedMovies)
                    }
                    return result.fulfill(response)
                }).catch({ (error) in
                    return result.reject(error)
                })
            case .getUpcomingMovies:
                self.requestForGetUpcomingMovies(parameters: parameters).done({ (responseFromServer) in
                    guard let response = responseFromServer as? UpcomingMoviesResponse else {
                        return result.reject(ErrorType.RequestServer.errorGettingUpcomingMovies)
                    }
                    return result.fulfill(response)
                }).catch({ (error) in
                    return result.reject(error)
                })
            case .getSearchingMovies:
                self.requestForGetSearchingMovies(parameters: parameters).done({ (responseFromServer) in
                    guard let response = responseFromServer as? SearchingMoviesResponse else {
                        return result.reject(ErrorType.RequestServer.errorGettingSearchingMovies)
                    }
                    return result.fulfill(response)
                }).catch({ (error) in
                    return result.reject(error)
                })
                
            case .getPopularTVShows:
                self.requestForGetPopularTVShows(parameters: parameters).done({ (responseFromServer) in
                    guard let response = responseFromServer as? PopularTVShowResponse else {
                        return result.reject(ErrorType.RequestServer.errorGettingPopularTVShows)
                    }
                    return result.fulfill(response)
                }).catch({ (error) in
                    return result.reject(error)
                })
            case .getTopRatedTVShows:
                self.requestForGetTopRatedTVShows(parameters: parameters).done({ (responseFromServer) in
                    guard let response = responseFromServer as? TopRatedTVShowResponse else {
                        return result.reject(ErrorType.RequestServer.errorGettingTopRatedTVShows)
                    }
                    return result.fulfill(response)
                }).catch({ (error) in
                    return result.reject(error)
                })
            case .getUpcomingTVShows:
                print()
                
            case .getSearchingTVShows:
                self.requestForGetSearchingTVShows(parameters: parameters).done({ (responseFromServer) in
                    guard let response = responseFromServer as? SearchingTVShowsResponse else {
                        return result.reject(ErrorType.RequestServer.errorGettingSearchingTVShows)
                    }
                    return result.fulfill(response)
                }).catch({ (error) in
                    return result.reject(error)
                })
                
            case .getVideosOfMovie:
                self.requestForGetVideosOfMovie(parameters: parameters).done({ (responseFromServer) in
                    guard let response = responseFromServer as? MovieAndTVShowsVideosResponse else {
                        return result.reject(ErrorType.RequestServer.errorGettingVideosOfMovie)
                    }
                    return result.fulfill(response)
                }).catch({ (error) in
                    return result.reject(ErrorType.RequestServer.errorGettingVideosOfMovie)
                })
            
            case .getVideosOfTVShow:
                self.requestForGetVideosOfTVShow(parameters: parameters).done({ (responseFromServer) in
                    guard let response = responseFromServer as? MovieAndTVShowsVideosResponse else {
                        return result.reject(ErrorType.RequestServer.errorGettingVideosOfTVShow)
                    }
                    return result.fulfill(response)
                }).catch({ (error) in
                    return result.reject(ErrorType.RequestServer.errorGettingVideosOfTVShow)
                })
            
            case .getConfigurationPaths:
                self.requestForConfigurationPaths(parameters: parameters).done({ (response) in
                    return result.fulfill(response)
                }).catch({ (error) in
                    return result.reject(error)
                })
                
            }
        }
    }
    //MARK: - Requests for Movies
    //Request Sample
    //https://api.themoviedb.org/3/movie/550?api_key=5b3a4ac9aee9d07e9f153eba21579ed9
    
    private func requestForGetPopularMovies(parameters: [String: Any]?) -> Promise<Any> {
        return Promise { result in
            if NetworkReachabilityManager()!.isReachable {
                let popularMoviesURL = self.baseURLServer + RequestUrl.getPopularMoviesURL.rawValue + self.apiKeyVariableForRequest
                
                Alamofire.request(popularMoviesURL, method: .get, parameters: parameters, encoding: JSONEncoding.default, headers: nil)
                    .validate(statusCode: 200..<505)
                    .response(completionHandler: { (response) in
                        guard let statusCode = response.response?.statusCode else { return result.reject(ErrorType.RequestServer.noValidAnswerFromServer)}
                        switch statusCode {
                        case (200...299):
                            do {
                                if let data = response.data {
                                    try self.deletePopularMoviesFromLocalStorage()
                                    let decoder = self.getJSONDecoder()
                                    let popularMoviesResponse = try decoder.decode(PopularMoviesResponse.self, from: data)
                                    guard let responseWithImageURL = self.setImageURLForMovies(response: popularMoviesResponse), let finalResponse = self.setTypeOfMovies(response: responseWithImageURL, to: .popular) else {
                                        return result.reject(ErrorType.RequestServer.errorGettingPopularMovies)
                                    }
                                    try self.saveMoviesIntoLocalStorage()
                                    return result.fulfill(finalResponse)
                                } else { //the response is nil
                                    return result.reject(ErrorType.RequestServer.noValidDataFromServer)
                                }
                            } catch let error {
                                print(error)
                                return result.reject(ErrorType.RequestServer.noValidDataFromServer)
                            }
                        case (300...505):
                            return result.reject(ErrorType.RequestServer.errorFromServer)
                        default:
                            return result.reject(ErrorType.RequestServer.unknownErrorFromServer)
                        }
                    })
            } else {
                return result.reject(ErrorType.RequestServer.noNetworkDetected)
            }
        }
    }
    
    private func requestForGetTopRatedMovies(parameters: [String: Any]?) -> Promise<Any> {
        return Promise { result in
            if NetworkReachabilityManager()!.isReachable {
                let popularMoviesURL = self.baseURLServer + RequestUrl.getTopRatedMoviesURL.rawValue + self.apiKeyVariableForRequest
                
                Alamofire.request(popularMoviesURL, method: .get, parameters: parameters, encoding: JSONEncoding.default, headers: nil)
                    .validate(statusCode: 200..<505)
                    .response(completionHandler: { (response) in
                        guard let statusCode = response.response?.statusCode else { return result.reject(ErrorType.RequestServer.noValidAnswerFromServer)}
                        switch statusCode {
                        case (200...299):
                            do {
                                if let data = response.data {
                                    try self.deleteTopRatedMoviesFromLocalStorage()
                                    let decoder = self.getJSONDecoder()
                                    let topRatedMoviesResponse = try decoder.decode(TopRatedMoviesResponse.self, from: data)
                                    guard let responseWithImageURL = self.setImageURLForMovies(response: topRatedMoviesResponse), let finalResponse = self.setTypeOfMovies(response: responseWithImageURL, to: .topRated) else {
                                        return result.reject(ErrorType.RequestServer.errorGettingTopRatedMovies)
                                    }
                                    try self.saveMoviesIntoLocalStorage()
                                    return result.fulfill(finalResponse)
                                } else { //the response is nil
                                    return result.reject(ErrorType.RequestServer.noValidDataFromServer)
                                }
                            } catch let error {
                                print(error)
                                return result.reject(ErrorType.RequestServer.noValidDataFromServer)
                            }
                        case (300...505):
                            return result.reject(ErrorType.RequestServer.errorFromServer)
                        default:
                            return result.reject(ErrorType.RequestServer.unknownErrorFromServer)
                        }
                    })
            } else {
                return result.reject(ErrorType.RequestServer.noNetworkDetected)
            }
        }
    }
    
    private func requestForGetUpcomingMovies(parameters: [String: Any]?) -> Promise<Any> {
        return Promise { result in
            if NetworkReachabilityManager()!.isReachable {
                let popularMoviesURL = self.baseURLServer + RequestUrl.getUpcomingMoviesURL.rawValue + self.apiKeyVariableForRequest
                
                Alamofire.request(popularMoviesURL, method: .get, parameters: parameters, encoding: JSONEncoding.default, headers: nil)
                    .validate(statusCode: 200..<505)
                    .response(completionHandler: { (response) in
                        guard let statusCode = response.response?.statusCode else { return result.reject(ErrorType.RequestServer.noValidAnswerFromServer)}
                        switch statusCode {
                        case (200...299):
                            do {
                                if let data = response.data {
                                    try self.deleteUpcomingMoviesFromLocalStorage()
                                    let decoder = self.getJSONDecoder()
                                    let upcomingMoviesResponse = try decoder.decode(UpcomingMoviesResponse.self, from: data)
                                    guard let responseWithImageURL = self.setImageURLForMovies(response: upcomingMoviesResponse), let responseWithUpcomingType = self.setTypeOfMovies(response: responseWithImageURL, to: .upcoming), let finalResponse = self.setDatesAttributeForEveryMovie(response: responseWithUpcomingType) else {
                                        return result.reject(ErrorType.RequestServer.errorGettingUpcomingMovies)
                                    }
                                    try self.saveMoviesIntoLocalStorage()
                                    return result.fulfill(finalResponse)
                                } else { //the response is nil
                                    return result.reject(ErrorType.RequestServer.noValidDataFromServer)
                                }
                            } catch let error {
                                print(error)
                                return result.reject(ErrorType.RequestServer.noValidDataFromServer)
                            }
                        case (300...505):
                            return result.reject(ErrorType.RequestServer.errorFromServer)
                        default:
                            return result.reject(ErrorType.RequestServer.unknownErrorFromServer)
                        }
                    })
            } else {
                return result.reject(ErrorType.RequestServer.noNetworkDetected)
            }
        }
    }
    
    private func requestForGetSearchingMovies(parameters: [String: Any]?) -> Promise<Any> {
        return Promise { result in
            if NetworkReachabilityManager()!.isReachable {
                guard let searchingString = parameters?["search"] as? String else {
                    return result.reject(ErrorType.RequestServer.wrongParametersInsideApp)
                }
                let getSearchingMoviesURL = self.baseURLServer + RequestUrl.getSearchingMoviesURL.rawValue + self.apiKeyVariableForRequest + "&query=" + searchingString
                
                Alamofire.request(getSearchingMoviesURL, method: .get, parameters: parameters, encoding: JSONEncoding.default, headers: nil)
                    .validate(statusCode: 200..<505)
                    .response(completionHandler: { (response) in
                        guard let statusCode = response.response?.statusCode else { return result.reject(ErrorType.RequestServer.noValidAnswerFromServer)}
                        switch statusCode {
                        case (200...299):
                            do {
                                if let data = response.data {
                                    let decoder = self.getJSONDecoder()
                                    let searchingMoviesResponse = try decoder.decode(SearchingMoviesResponse.self, from: data)
                                    guard let responseWithImageURL = self.setImageURLForMovies(response: searchingMoviesResponse), let finalResponse = self.setTypeOfMovies(response: responseWithImageURL, to: .searched) else {
                                        return result.reject(ErrorType.RequestServer.errorGettingSearchingMovies)
                                    }
                                    return result.fulfill(finalResponse)
                                } else { //the response is nil
                                    return result.reject(ErrorType.RequestServer.noValidDataFromServer)
                                }
                            } catch let error {
                                print(error)
                                return result.reject(ErrorType.RequestServer.noValidDataFromServer)
                            }
                        case (300...505):
                            return result.reject(ErrorType.RequestServer.errorFromServer)
                        default:
                            return result.reject(ErrorType.RequestServer.unknownErrorFromServer)
                        }
                    })
            } else {
                return result.reject(ErrorType.RequestServer.noNetworkDetected)
            }
        }
    }
    
    //MARK: - Requests for TVShows
    
    private func requestForGetPopularTVShows(parameters: [String: Any]?) -> Promise<Any> {
        return Promise { result in
            if NetworkReachabilityManager()!.isReachable {
                let popularMoviesURL = self.baseURLServer + RequestUrl.getPopularTVShowsURL.rawValue + self.apiKeyVariableForRequest
                
                Alamofire.request(popularMoviesURL, method: .get, parameters: parameters, encoding: JSONEncoding.default, headers: nil)
                    .validate(statusCode: 200..<505)
                    .response(completionHandler: { (response) in
                        guard let statusCode = response.response?.statusCode else { return result.reject(ErrorType.RequestServer.noValidAnswerFromServer)}
                        switch statusCode {
                        case (200...299):
                            do {
                                if let data = response.data {
                                    try self.deletePopularTVShowsFromLocalStorage()
                                    let decoder = self.getJSONDecoder()
                                    let popularTVShowsResponse = try decoder.decode(PopularTVShowResponse.self, from: data)
                                    guard let responseWithImageURL = self.setImageURLForTVShows(response: popularTVShowsResponse), let finalResponse = self.setTypeOfTVShows(response: responseWithImageURL, to: .popular) else {
                                        return result.reject(ErrorType.RequestServer.errorGettingPopularTVShows)
                                    }
                                    try self.saveTVShowsIntoLocalStorage()
                                    return result.fulfill(finalResponse)
                                } else { //the response is nil
                                    return result.reject(ErrorType.RequestServer.noValidDataFromServer)
                                }
                            } catch let error {
                                print(error)
                                return result.reject(ErrorType.RequestServer.noValidDataFromServer)
                            }
                        case (300...505):
                            return result.reject(ErrorType.RequestServer.errorFromServer)
                        default:
                            return result.reject(ErrorType.RequestServer.unknownErrorFromServer)
                        }
                    })
            } else {
                return result.reject(ErrorType.RequestServer.noNetworkDetected)
            }
        }
    }
    
    private func requestForGetTopRatedTVShows(parameters: [String: Any]?) -> Promise<Any> {
        return Promise { result in
            if NetworkReachabilityManager()!.isReachable {
                let popularMoviesURL = self.baseURLServer + RequestUrl.getTopRatedTVShowsURL.rawValue + self.apiKeyVariableForRequest
                
                Alamofire.request(popularMoviesURL, method: .get, parameters: parameters, encoding: JSONEncoding.default, headers: nil)
                    .validate(statusCode: 200..<505)
                    .response(completionHandler: { (response) in
                        guard let statusCode = response.response?.statusCode else { return result.reject(ErrorType.RequestServer.noValidAnswerFromServer)}
                        switch statusCode {
                        case (200...299):
                            do {
                                if let data = response.data {
                                    try self.deleteTopRatedTVShowsFromLocalStorage()
                                    let decoder = self.getJSONDecoder()
                                    let topRatedTVShowsResponse = try decoder.decode(TopRatedTVShowResponse.self, from: data)
                                    guard let responseWithImageURL = self.setImageURLForTVShows(response: topRatedTVShowsResponse), let finalResponse = self.setTypeOfTVShows(response: responseWithImageURL, to: .topRated) else {
                                        return result.reject(ErrorType.RequestServer.errorGettingTopRatedTVShows)
                                    }
                                    try self.saveTVShowsIntoLocalStorage()
                                    return result.fulfill(finalResponse)
                                } else { //the response is nil
                                    return result.reject(ErrorType.RequestServer.noValidDataFromServer)
                                }
                            } catch let error {
                                print(error)
                                return result.reject(ErrorType.RequestServer.noValidDataFromServer)
                            }
                        case (300...505):
                            return result.reject(ErrorType.RequestServer.errorFromServer)
                        default:
                            return result.reject(ErrorType.RequestServer.unknownErrorFromServer)
                        }
                    })
            } else {
                return result.reject(ErrorType.RequestServer.noNetworkDetected)
            }
        }
    }
    
    private func requestForGetSearchingTVShows(parameters: [String: Any]?) -> Promise<Any> {
        return Promise { result in
            if NetworkReachabilityManager()!.isReachable {
                guard let searchingString = parameters?["search"] as? String else {
                    return result.reject(ErrorType.RequestServer.wrongParametersInsideApp)
                }
                let getSearchingTVShowsURL = self.baseURLServer + RequestUrl.getSearchingTVShowsURL.rawValue + self.apiKeyVariableForRequest + "&query=" + searchingString
                
                Alamofire.request(getSearchingTVShowsURL, method: .get, parameters: parameters, encoding: JSONEncoding.default, headers: nil)
                    .validate(statusCode: 200..<505)
                    .response(completionHandler: { (response) in
                        guard let statusCode = response.response?.statusCode else { return result.reject(ErrorType.RequestServer.noValidAnswerFromServer)}
                        switch statusCode {
                        case (200...299):
                            do {
                                if let data = response.data {
                                    let decoder = self.getJSONDecoder()
                                    let searchingTVShowsResponse = try decoder.decode(SearchingTVShowsResponse.self, from: data)
                                    guard let responseWithImageURL = self.setImageURLForTVShows(response: searchingTVShowsResponse), let finalResponse = self.setTypeOfTVShows(response: responseWithImageURL, to: .searched) else {
                                        return result.reject(ErrorType.RequestServer.errorGettingSearchingTVShows)
                                    }
                                    return result.fulfill(finalResponse)
                                } else { //the response is nil
                                    return result.reject(ErrorType.RequestServer.noValidDataFromServer)
                                }
                            } catch let error {
                                print(error)
                                return result.reject(ErrorType.RequestServer.noValidDataFromServer)
                            }
                        case (300...505):
                            return result.reject(ErrorType.RequestServer.errorFromServer)
                        default:
                            return result.reject(ErrorType.RequestServer.unknownErrorFromServer)
                        }
                    })
            } else {
                return result.reject(ErrorType.RequestServer.noNetworkDetected)
            }
        }
    }
    
    //MARK: - Requests for get videos of Movies or TVShows
    
    private func requestForGetVideosOfMovie(parameters: [String: Any]?) -> Promise<Any> {
        return Promise { result in
            if NetworkReachabilityManager()!.isReachable {
                guard let validParameters = parameters, let movieId = validParameters["movieId"] as? String else {
                    return result.reject(ErrorType.RequestServer.wrongParametersInsideApp)
                }
                let urlWithMovieId = RequestUrl.getVideosOfMovieURL.rawValue.replacingOccurrences(of: "{movie_id}", with: movieId)
                let getVideosOfMovieURL = self.baseURLServer + urlWithMovieId + self.apiKeyVariableForRequest
                Alamofire.request(getVideosOfMovieURL, method: .get, parameters: parameters, encoding: JSONEncoding.default, headers: nil)
                    .validate(statusCode: 200..<505)
                    .response(completionHandler: { (response) in
                        guard let statusCode = response.response?.statusCode else { return result.reject(ErrorType.RequestServer.noValidAnswerFromServer)}
                        switch statusCode {
                        case (200...299):
                            do {
                                if let data = response.data {
                                    let videosForMovieResponse = try JSONDecoder().decode(MovieAndTVShowsVideosResponse.self, from: data)
                                    return result.fulfill(videosForMovieResponse)
                                } else { //the response is nil
                                    return result.reject(ErrorType.RequestServer.noValidDataFromServer)
                                }
                            } catch let error {
                                print(error)
                                return result.reject(ErrorType.RequestServer.noValidDataFromServer)
                            }
                        case (300...505):
                            return result.reject(ErrorType.RequestServer.errorFromServer)
                        default:
                            return result.reject(ErrorType.RequestServer.unknownErrorFromServer)
                        }
                    })
            } else {
                return result.reject(ErrorType.RequestServer.noNetworkDetected)
            }
        }
    }
    
    private func requestForGetVideosOfTVShow(parameters: [String: Any]?) -> Promise<Any> {
        return Promise { result in
            if NetworkReachabilityManager()!.isReachable {
                guard let validParameters = parameters, let tvId = validParameters["tvShowId"] as? String else {
                    return result.reject(ErrorType.RequestServer.wrongParametersInsideApp)
                }
                let urlWithTVId = RequestUrl.getVideosOfTVShowURL.rawValue.replacingOccurrences(of: "{tv_id}", with: tvId)
                let getVideosOfMovieURL = self.baseURLServer + urlWithTVId + self.apiKeyVariableForRequest
                Alamofire.request(getVideosOfMovieURL, method: .get, parameters: parameters, encoding: JSONEncoding.default, headers: nil)
                    .validate(statusCode: 200..<505)
                    .response(completionHandler: { (response) in
                        guard let statusCode = response.response?.statusCode else { return result.reject(ErrorType.RequestServer.noValidAnswerFromServer)}
                        switch statusCode {
                        case (200...299):
                            do {
                                if let data = response.data {
                                    let videosForTVShowResponse = try JSONDecoder().decode(MovieAndTVShowsVideosResponse.self, from: data)
                                    return result.fulfill(videosForTVShowResponse)
                                } else { //the response is nil
                                    return result.reject(ErrorType.RequestServer.noValidDataFromServer)
                                }
                            } catch let error {
                                print(error)
                                return result.reject(ErrorType.RequestServer.noValidDataFromServer)
                            }
                        case (300...505):
                            return result.reject(ErrorType.RequestServer.errorFromServer)
                        default:
                            return result.reject(ErrorType.RequestServer.unknownErrorFromServer)
                        }
                    })
            } else {
                return result.reject(ErrorType.RequestServer.noNetworkDetected)
            }
        }
    }
    
    //MARK: - Request for get configuration info.
    
    private func requestForConfigurationPaths(parameters: [String: Any]?) -> Promise<Any> {
        return Promise { result in
            if NetworkReachabilityManager()!.isReachable {
                let configurationURL = self.baseURLServer + RequestUrl.getConfigurationPathsURL.rawValue + self.apiKeyVariableForRequest
                Alamofire.request(configurationURL, method: .get, parameters: parameters, encoding: JSONEncoding.default, headers: nil)
                    .validate(statusCode: 200..<505)
                    .response(completionHandler: { (response) in
                        guard let statusCode = response.response?.statusCode else { return result.reject(ErrorType.RequestServer.noValidAnswerFromServer)}
                        switch statusCode {
                        case (200...299):
                            do {
                                if let data = response.data {
                                    print(String.init(data: data, encoding: String.Encoding.utf8) ?? "No valid data")
                                    let configurationResponse = try JSONDecoder().decode(ConfigurationRequestServerResponse.self, from: data)
                                    self.configurationResponse = configurationResponse
                                    return result.fulfill(true)
                                } else { //the response is nil
                                    return result.reject(ErrorType.RequestServer.noValidDataFromServer)
                                }
                            } catch let error {
                                print(error)
                                return result.reject(ErrorType.RequestServer.noValidDataFromServer)
                            }
                        case (300...505):
                            return result.reject(ErrorType.RequestServer.errorFromServer)
                        default:
                            return result.reject(ErrorType.RequestServer.unknownErrorFromServer)
                        }
                    })
            } else {
                return result.reject(ErrorType.RequestServer.noNetworkDetected)
            }
        }
    }
    
}


extension NetworkManager {
    
    private func getJSONDecoder() -> JSONDecoder {
        let decoder = JSONDecoder()
        decoder.userInfo[CodingUserInfoKey.context!] = self.getManagedObjectConext()
        return decoder
    }
    
    private func getManagedObjectConext() -> NSManagedObjectContext {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        return appDelegate.persistentContainer.viewContext
    }
    
    private func saveMoviesIntoLocalStorage() throws {
        try self.getManagedObjectConext().save()
    }
    
    private func saveTVShowsIntoLocalStorage() throws {
        try self.getManagedObjectConext().save()
    }
    
    private func deletePopularMoviesFromLocalStorage() throws {
        try CoreDataManager.removeAllMoviesOfType(type: .popular)
    }
    
    private func deleteTopRatedMoviesFromLocalStorage() throws {
        try CoreDataManager.removeAllMoviesOfType(type: .topRated)
    }
    
    private func deleteUpcomingMoviesFromLocalStorage() throws {
        try CoreDataManager.removeAllMoviesOfType(type: .upcoming)
    }
    
    private func deletePopularTVShowsFromLocalStorage() throws {
        try CoreDataManager.removeAllTVShowsOfType(type: .popular)
    }
    
    private func deleteTopRatedTVShowsFromLocalStorage() throws {
        try CoreDataManager.removeAllTVShowsOfType(type: .topRated)
    }
    
    private func setImageURLForMovies<GenericResponse: BasicResponseForMovies>(response: GenericResponse) -> GenericResponse? {
        guard let imageBasePath = self.configurationResponse?.images?.base_url, let backdropSizes = self.configurationResponse?.images?.backdrop_sizes, let posterSizes = self.configurationResponse?.images?.poster_sizes, let movies = response.results else {
            return nil
        }
        let backdropSize = backdropSizes.indices.contains((backdropSizes.count - 1) / 2) ? backdropSizes[(backdropSizes.count - 1) / 2] : "original"
        let posterSize = posterSizes.indices.contains((posterSizes.count - 1) / 2) ? posterSizes[(posterSizes.count - 1) / 2] : "original"
        var finalMovies: [Movie] = [Movie]()
        for movie in movies {
            guard let posterPath = movie.poster_path, let backdropPath = movie.backdrop_path else {
                break
            }
            movie.backdrop_path = imageBasePath + backdropSize + backdropPath
            movie.poster_path = imageBasePath + posterSize + posterPath
            finalMovies.append(movie)
        }
        var finalResponse = response
        finalResponse.results = finalMovies
        return finalResponse
    }
    
    private func setTypeOfMovies<GenericResponse: BasicResponseForMovies>(response: GenericResponse, to type: TypeOfMovies) -> GenericResponse? {
        guard let movies = response.results else {
            return nil
        }
        var finalMovies: [Movie] = [Movie]()
        for movie in movies {
            movie.type = Int16(type.rawValue)
            finalMovies.append(movie)
        }
        var finalResponse = response
        finalResponse.results = finalMovies
        return finalResponse
    }
    
    private func setDatesAttributeForEveryMovie(response: UpcomingMoviesResponse) -> UpcomingMoviesResponse? {
        guard let movies = response.results else {
            return nil
        }
        guard let dates = response.dates else {
            return nil
        }
        var finalMovies: [Movie] = [Movie]()
        let context = self.getManagedObjectConext()
        let entity = NSEntityDescription.entity(forEntityName: "Dates", in: context)!
        for movie in movies {
            let datesForMovie = Dates.init(entity: entity, context: context, maximum: dates.maximum, minimum: dates.minimum)
            movie.hasThis = datesForMovie
            finalMovies.append(movie)
        }
        var finalResponse = response
        finalResponse.results = finalMovies
        return finalResponse
    }
    
    
    private func setImageURLForTVShows<GenericResponse: BasicResponseForTVShows>(response: GenericResponse) -> GenericResponse? {
        guard let imageBasePath = self.configurationResponse?.images?.base_url, let backdropSizes = self.configurationResponse?.images?.backdrop_sizes, let posterSizes = self.configurationResponse?.images?.poster_sizes, let tvshows = response.results else {
            return nil
        }
        let backdropSize = backdropSizes.indices.contains((backdropSizes.count - 1) / 2) ? backdropSizes[(backdropSizes.count - 1) / 2] : "original"
        let posterSize = posterSizes.indices.contains((posterSizes.count - 1) / 2) ? posterSizes[(posterSizes.count - 1) / 2] : "original"
        var finalTVShows: [TVShow] = [TVShow]()
        for tvshow in tvshows {
            guard let posterPath = tvshow.poster_path, let backdropPath = tvshow.backdrop_path else {
                break
            }
            tvshow.backdrop_path = imageBasePath + backdropSize + backdropPath
            tvshow.poster_path = imageBasePath + posterSize + posterPath
            finalTVShows.append(tvshow)
        }
        var finalResponse = response
        finalResponse.results = finalTVShows
        return finalResponse
    }
    
    private func setTypeOfTVShows<GenericResponse: BasicResponseForTVShows>(response: GenericResponse, to type: TypeOfTVShows) -> GenericResponse? {
        guard let tvshows = response.results else {
            return nil
        }
        var finalTVShows: [TVShow] = [TVShow]()
        for tvshow in tvshows {
            tvshow.type = Int16(type.rawValue)
            finalTVShows.append(tvshow)
        }
        var finalResponse = response
        finalResponse.results = finalTVShows
        return finalResponse
    }
    
    
}
