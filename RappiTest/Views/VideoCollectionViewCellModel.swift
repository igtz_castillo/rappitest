//
//  MovieVideoCollectionViewCellModel.swift
//  RappiTest
//
//  Created by Israel Gtz on 8/12/19.
//  Copyright © 2019 Rohkeus Systems. All rights reserved.
//

import Foundation

class VideoCollectionViewCellModel {
    
    enum LoadVideo {
        struct Request {
            var videoURL: String
        }
    }
    
}
