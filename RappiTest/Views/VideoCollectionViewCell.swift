//
//  VideoCollectionViewCell.swift
//  RappiTest
//
//  Created by Israel Gtz on 8/12/19.
//  Copyright © 2019 Rohkeus Systems. All rights reserved.
//

import Foundation
import UIKit
import WebKit

class VideoCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var mainWebView: WKWebView!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!

    override func layoutSubviews() {
        super.layoutSubviews()
        self.mainWebView.navigationDelegate = self
    }
    
    func loadVideo(request: VideoCollectionViewCellModel.LoadVideo.Request) {
        self.mainWebView.alpha = 0.0
        self.activityIndicator.alpha = 1.0
        
        guard let url = URL.init(string: request.videoURL) else {
            return
        }
        let urlRequest = URLRequest.init(url: url)
        self.mainWebView.scrollView.isScrollEnabled = false
        self.mainWebView.load(urlRequest)
        self.activityIndicator.startAnimating()
    }


}

extension VideoCollectionViewCell: WKNavigationDelegate {

    
    
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        self.activityIndicator.stopAnimating()
        UIView.animate(withDuration: 0.15) {
            self.activityIndicator.alpha = 0.0
            self.mainWebView.alpha = 1.0
        }
    }
    
}
