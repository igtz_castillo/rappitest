//
//  TopRatedTVShowsViewControllerViewModel.swift
//  RappiTest
//
//  Created by Israel Gtz on 8/13/19.
//  Copyright © 2019 Rohkeus Systems. All rights reserved.
//

import Foundation
import UIKit
import PromiseKit

protocol TopRatedTVShowsViewControllerViewModelDelegate: PresentLoaderLogic {
    func reloadTable()
    func endRefreshingTableView()
    func pushViewController(viewController: UIViewController)
}

class TopRatedTVShowsViewControllerViewModel {
    
    var allTVShows: [TVShow] = [TVShow]() {
        didSet {
            self.delegate?.reloadTable()
            self.delegate?.endRefreshingTableView()
        }
    }
    var filteredTVShows: [TVShow] = [TVShow]()
    var isSearching: Bool = false
    var delegate: TopRatedTVShowsViewControllerViewModelDelegate?
    
    func getTVShowsData() {
        self.delegate?.showLoader(request: PresentLoaderLogicModel.ShowLoader.Request.init(message: .fetchingTopRateTVShows))
        NetworkManager.shared.doRequest(type: .getConfigurationPaths, parameters: nil).then { (configurationResponse) -> Promise<Any> in
            NetworkManager.shared.doRequest(type: .getTopRatedTVShows, parameters: nil)
            }.done { (responseFromServer) in
                guard let topRatedTVShowsResponse = responseFromServer as? TopRatedTVShowResponse, let tvShows = topRatedTVShowsResponse.results else {
                    self.delegate?.hideLoader()
                    self.delegate?.showMessage(request: PresentLoaderLogicModel.ShowMessage.Request.init(message: Messages.errorParsingData.rawValue, title: nil))
                    self.delegate?.endRefreshingTableView()
                    return
                }
                self.allTVShows = tvShows
                self.delegate?.hideLoader()
            }.catch { (error) in
                let responseFromCoreData = CoreDataManager.getTVShowsOfSomeType(request: CoreDataManagerModel.GetTVShowsOfSomeType.Request.init(type: .topRated))
                if responseFromCoreData.error != nil {
                    self.delegate?.hideLoader()
                    self.delegate?.showMessage(request: PresentLoaderLogicModel.ShowMessage.Request.init(message: Messages.noLocalStorageData.rawValue, title: nil))
                    self.delegate?.endRefreshingTableView()
                    return
                } else {
                    self.allTVShows = responseFromCoreData.tvshows
                    self.delegate?.hideLoader()
                    if responseFromCoreData.tvshows.count == 0 {
                        self.delegate?.showMessage(request: PresentLoaderLogicModel.ShowMessage.Request.init(message: Messages.noLocalStorageData.rawValue, title: nil))
                    }
                }
        }
    }
    
    func tableViewDidSelectRowAt(request: TopRatedTVShowsViewControllerModel.TableViewDidSelectRowAt.Request) {
        var tvShowData: TVShow
        if self.isSearching {
            tvShowData = self.filteredTVShows[request.indexPath.row]
        } else {
            tvShowData = self.allTVShows[request.indexPath.row]
        }
        self.delegate?.pushViewController(viewController: self.instanceOfMovieDetailViewController(tvShowData: tvShowData))
    }
    
    private func instanceOfMovieDetailViewController(tvShowData: TVShow) -> TVShowDetailViewController {
        let storyboard = UIStoryboard(name: "TVShowDetailStoryboard", bundle: nil)
        let viewController = storyboard.instantiateViewController(withIdentifier: "TVShowDetailViewControllerID") as! TVShowDetailViewController
        viewController.viewModel.tvShowData = tvShowData
        return viewController
    }
    
    func numberOfRowsInSection(request: TopRatedTVShowsViewControllerModel.NumberOfRowsInSection.Request) -> TopRatedTVShowsViewControllerModel.NumberOfRowsInSection.Response {
        if self.isSearching {
            return TopRatedTVShowsViewControllerModel.NumberOfRowsInSection.Response.init(numberOfElements: self.filteredTVShows.count)
        } else {
            return TopRatedTVShowsViewControllerModel.NumberOfRowsInSection.Response.init(numberOfElements: self.allTVShows.count)
        }
    }
    
    func tableViewCellForRowAt(request: TopRatedTVShowsViewControllerModel.TableViewCellForRowAt.Request) -> TopRatedTVShowsViewControllerModel.TableViewCellForRowAt.Response {
        let cell = request.tableView.dequeueReusableCell(withIdentifier: request.cellIdentifier, for: request.indexPath) as! TVShowTableViewCell
        let setAppearanceCellRequest = TVShowTableViewCellModel.SetAppearance.Request.init(tvShowData: self.isSearching ? self.filteredTVShows[request.indexPath.row] : self.allTVShows[request.indexPath.row])
        cell.setAppearance(request: setAppearanceCellRequest)
        return TopRatedTVShowsViewControllerModel.TableViewCellForRowAt.Response.init(cell: cell)
    }
    
    func searchBarTextDidChange(request: TopRatedTVShowsViewControllerModel.SearchBarTextDidChange.Request) {
        if request.searchingText == "" {
            self.isSearching = false
        } else {
            self.isSearching = true
        }
        self.filterElements(searchingText: request.searchingText)
    }
    
    private func filterElements(searchingText: String) {
        if searchingText == "" {
            self.filteredTVShows = self.allTVShows
        } else {
            var searchResults = self.allTVShows
            let strippedString = searchingText.trimmingCharacters(in: .whitespaces)
            var searchItems: [String] = Array()
            if strippedString.count > 0 {
                searchItems = strippedString.components(separatedBy: " ")
            }
            var andMatchPredictaes: [NSCompoundPredicate] = Array()
            for searchString in searchItems {
                var searchItemsPredicate: [NSPredicate] = Array()
                
                let lhs: NSExpression = NSExpression(forKeyPath: "name")
                let rhs: NSExpression = NSExpression(forConstantValue: searchString)
                let finalPredicate: NSComparisonPredicate = NSComparisonPredicate(leftExpression: lhs,
                                                                                  rightExpression: rhs,
                                                                                  modifier: .direct,
                                                                                  type: .contains,
                                                                                  options: NSComparisonPredicate.Options.caseInsensitive)
                searchItemsPredicate.append(finalPredicate)
                let orMatchPredicates: NSCompoundPredicate = NSCompoundPredicate(orPredicateWithSubpredicates: searchItemsPredicate)
                andMatchPredictaes.append(orMatchPredicates)
            }
            
            let finalCompoundPredicate = NSCompoundPredicate(andPredicateWithSubpredicates: andMatchPredictaes)
            let nsArraySearchResults = searchResults as NSArray
            searchResults = (nsArraySearchResults.filtered(using: finalCompoundPredicate) as? [TVShow]) ?? Array()
            self.filteredTVShows = searchResults
        }
        self.delegate?.reloadTable()
    }
    
    
}
