//
//  TVShowsViewController.swift
//  RappiTest
//
//  Created by Israel Gtz on 8/9/19.
//  Copyright © 2019 Rohkeus Systems. All rights reserved.
//

import Foundation
import UIKit

class TVShowsViewController: UIViewController {
    
    @IBOutlet weak var segmentedControl: UISegmentedControl!
    @IBOutlet weak var childViewControllerContainerView: UIView!
    
    lazy var viewModel: TVShowsViewControllerViewModel = {
        return TVShowsViewControllerViewModel()
    }()
    
    lazy var popularTVShowsViewController: PopularTVShowsViewController = {
        let storyboard = UIStoryboard(name: "TVShowsStoryboard", bundle: nil)
        var viewController = storyboard.instantiateViewController(withIdentifier: "PopularTVShowsViewControllerID") as! PopularTVShowsViewController
        return viewController
    }()
    
    lazy var topRtdTVShowsViewController: TopRatedTVShowsViewController = {
        let storyboard = UIStoryboard(name: "TVShowsStoryboard", bundle: nil)
        var viewController = storyboard.instantiateViewController(withIdentifier: "TopRatedTVShowsViewControllerID") as! TopRatedTVShowsViewController
        return viewController
    }()

//    lazy var upComingMoviesViewController: UpcomingMoviesViewController = {
//        let storyboard = UIStoryboard(name: "TVShowsStoryboard", bundle: nil)
//        var viewController = storyboard.instantiateViewController(withIdentifier: "UpcomingMoviesViewControllerID") as! UpcomingMoviesViewController
//        return viewController
//    }()
    
    lazy var searchingTVShowsViewController: SearchingTVShowsViewController = {
        let storyboard = UIStoryboard(name: "TVShowsStoryboard", bundle: nil)
        var viewController = storyboard.instantiateViewController(withIdentifier: "SearchingTVShowsViewControllerID") as! SearchingTVShowsViewController
        return viewController
    }()
    
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
        self.setUpViewModel()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.setUpViewModel()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setSegmentedControl()
        self.hideKeyboardWhenTappedAround()
    }
    
    private func setUpViewModel() {
        
    }
    
    private func setSegmentedControl() {
        self.segmentedControl.addTarget(self, action: #selector(selectionDidChange), for: .valueChanged)
        self.segmentedControl.selectedSegmentIndex = 0
        self.segmentedControl.sendActions(for: .valueChanged)
    }
    
    @objc func selectionDidChange() {
        if self.viewModel.previousSegmentedControlIndexSelected == 0 {
            if self.segmentedControl.selectedSegmentIndex == 1 {
                self.pushNavigationItemTitle(nameOfScreen: .topRatedTVShows, direction: .fromRight)
                self.viewModel.changeViewController(add: self.topRtdTVShowsViewController, remove: self.popularTVShowsViewController, to: self, in: self.childViewControllerContainerView)
            } else
            if self.segmentedControl.selectedSegmentIndex == 2 {
                self.pushNavigationItemTitle(nameOfScreen: .searchingTVShows, direction: .fromRight)
                self.viewModel.changeViewController(add: self.searchingTVShowsViewController, remove: self.popularTVShowsViewController, to: self, in: self.childViewControllerContainerView)
            }
        } else
        if self.viewModel.previousSegmentedControlIndexSelected == 1 {
            if self.segmentedControl.selectedSegmentIndex == 0 {
                self.pushNavigationItemTitle(nameOfScreen: .popularTVShows, direction: .fromLeft)
                self.viewModel.changeViewController(add: self.popularTVShowsViewController, remove: self.topRtdTVShowsViewController, to: self, in: self.childViewControllerContainerView)
            } else
            if self.segmentedControl.selectedSegmentIndex == 2 {
                self.pushNavigationItemTitle(nameOfScreen: .searchingTVShows, direction: .fromRight)
                self.viewModel.changeViewController(add: self.searchingTVShowsViewController, remove: self.topRtdTVShowsViewController, to: self, in: self.childViewControllerContainerView)
            }
        } else
        if self.viewModel.previousSegmentedControlIndexSelected == 2 {
            if self.segmentedControl.selectedSegmentIndex == 0 {
                self.pushNavigationItemTitle(nameOfScreen: .popularTVShows, direction: .fromLeft)
                self.viewModel.changeViewController(add: self.popularTVShowsViewController, remove: self.searchingTVShowsViewController, to: self, in: self.childViewControllerContainerView)
            } else
            if self.segmentedControl.selectedSegmentIndex == 1 {
                self.pushNavigationItemTitle(nameOfScreen: .topRatedTVShows, direction: .fromLeft)
                self.viewModel.changeViewController(add: self.topRtdTVShowsViewController, remove: self.searchingTVShowsViewController, to: self, in: self.childViewControllerContainerView)
            }
        }
        self.viewModel.previousSegmentedControlIndexSelected = self.segmentedControl.selectedSegmentIndex
    }
    
    private func pushNavigationItemTitle(nameOfScreen: NameOfTVShowsScreens, direction: CATransitionSubtype) {
        let pushTextAnimation = CATransition()
        pushTextAnimation.duration = 0.15
        pushTextAnimation.type = .push
        pushTextAnimation.subtype = direction
        self.navigationController?.navigationBar.layer.add(pushTextAnimation, forKey: "pushText")
        self.navigationItem.title = nameOfScreen.rawValue
    }
    
}
