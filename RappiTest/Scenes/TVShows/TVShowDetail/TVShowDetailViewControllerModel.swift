//
//  TVShowDetailViewControllerModel.swift
//  RappiTest
//
//  Created by Israel Gtz on 8/13/19.
//  Copyright © 2019 Rohkeus Systems. All rights reserved.
//

import Foundation
import UIKit

class TVShowDetailViewControllerModel {
    
    enum GetVideosOfTVShow {
        struct Request {
            var tvShowId: String
        }
        struct Response {
            var videoURLOfTVShow: [String]
        }
    }
    
    enum CollectionViewCellForItemAtIndexPath {
        
        struct Request {
            var collectionView: UICollectionView
            var indexPath: IndexPath
            var collectionViewCellIdentifier: String
        }
        
        struct Response {
            var collectionViewCell: VideoCollectionViewCell
        }
        
    }
    
    enum CollectionViewNumberOfItemsInSection {
        struct Response {
            var numberOfItems: Int
        }
    }
    
}
