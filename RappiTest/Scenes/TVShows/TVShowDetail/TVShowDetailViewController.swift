//
//  TVShowDetailViewController.swift
//  RappiTest
//
//  Created by Israel Gtz on 8/13/19.
//  Copyright © 2019 Rohkeus Systems. All rights reserved.
//

import Foundation
import UIKit

class TVShowDetailViewController: UIViewController {
    
    @IBOutlet weak var videosCollectionView: UICollectionView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var averageVoteLabel: UILabel!
    @IBOutlet weak var overviewLabel: UILabel!
    @IBOutlet weak var noAvailableVideosLabel: UILabel!
    @IBOutlet weak var rightArrow: UIView!
    
    
    lazy var viewModel: TVShowDetailViewControllerViewModel = {
        return TVShowDetailViewControllerViewModel()
    }()
    private let kCollectionViewCellId = "collectionCellId"
    
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
        self.setUpViewModel()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.setUpViewModel()
    }
    
    private func setUpViewModel() {
        self.viewModel.delegate = self
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setUpVideosCollectionView()
        self.getVideosOfMovie()
        self.setUpUI()
        self.hideKeyboardWhenTappedAround()
    }
    
    private func setUpVideosCollectionView() {
        self.videosCollectionView.register(UINib.init(nibName: "VideoCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: self.kCollectionViewCellId)
        self.videosCollectionView.delegate = self
        self.videosCollectionView.dataSource = self
        self.videosCollectionView.isPagingEnabled = true
    }
    
    private func getVideosOfMovie() {
        self.viewModel.getVideosOfMovie()
    }
    
    private func setUpUI() {
        guard let tvShowData = self.viewModel.tvShowData else {
            return
        }
        self.titleLabel.text = tvShowData.name
        self.averageVoteLabel.text = ("\(tvShowData.vote_average)")
        self.overviewLabel.text = tvShowData.overview
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        self.videosCollectionView.collectionViewLayout.invalidateLayout()
    }
    
}

extension TVShowDetailViewController: UICollectionViewDelegate {
    
}

extension TVShowDetailViewController: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.viewModel.collectionViewNumberOfItemsInSection().numberOfItems
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let request = TVShowDetailViewControllerModel.CollectionViewCellForItemAtIndexPath.Request.init(collectionView: collectionView, indexPath: indexPath, collectionViewCellIdentifier: self.kCollectionViewCellId)
        return self.viewModel.collectionViewCellForItemAtIndexPath(request: request).collectionViewCell
    }
}

extension TVShowDetailViewController: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize.init(width: self.view.safeAreaLayoutGuide.layoutFrame.width, height: self.videosCollectionView.frame.height)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets.init(top: 0.0, left: 0.0, bottom: 0.0, right: 0.0)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
}

extension TVShowDetailViewController: TVShowDetailViewControllerViewModelDelegate {
    func reloadCollectionView() {
        self.videosCollectionView.reloadData()
    }
    
    func showNoVideosLabel() {
        UIView.animate(withDuration: 0.25, animations: {
            self.noAvailableVideosLabel.alpha = 1.0
        }, completion: nil)
    }
    
    func animateArrowForMoreVideos() {
        UIView.animate(withDuration: 0.35, animations: {
            self.rightArrow.alpha = 1.0
        }) { (finished) in
            if finished {
                UIView.animate(withDuration: 0.2, animations: {
                    self.rightArrow.alpha = 0.0
                }, completion: { (finished) in
                    if finished {
                        UIView.animate(withDuration: 0.35, animations: {
                            self.rightArrow.alpha = 1.0
                        }, completion: { (finished) in
                            if finished {
                                UIView.animate(withDuration: 0.2, animations: {
                                    self.rightArrow.alpha = 0.0
                                }, completion: nil)
                            }
                        })
                    }
                })
            }
        }
    }
    
}
