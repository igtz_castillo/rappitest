//
//  TVShowDetailViewControllerViewModel.swift
//  RappiTest
//
//  Created by Israel Gtz on 8/13/19.
//  Copyright © 2019 Rohkeus Systems. All rights reserved.
//

import Foundation
import UIKit

protocol TVShowDetailViewControllerViewModelDelegate {
    func reloadCollectionView()
    func showNoVideosLabel()
    func animateArrowForMoreVideos()
}


class TVShowDetailViewControllerViewModel {
    
    var tvShowData: TVShow? = nil
    var videosURLForTVShow: [String] = [String]() {
        didSet {
            self.delegate?.reloadCollectionView()
            if self.videosURLForTVShow.count == 0 {
                self.delegate?.showNoVideosLabel()
            } else
            if self.videosURLForTVShow.count > 1 {
                self.delegate?.animateArrowForMoreVideos()
            }
        }
    }
    var delegate: TVShowDetailViewControllerViewModelDelegate?
    
    func getVideosOfMovie() {
        guard let tvshowData = self.tvShowData else {
            return
        }
        let tvShowId = ("\(tvshowData.id)")
        let params: [String: Any] = ["tvShowId": tvShowId]
        NetworkManager.shared.doRequest(type: .getVideosOfTVShow, parameters: params).done { (response) in
            guard let videosTVShowResponse = response as? MovieAndTVShowsVideosResponse else {
                return
            }
            self.videosURLForTVShow = self.createURLOfVideosForTVShow(videosInfo: videosTVShowResponse.results ?? [VideoForItem]())
            }.catch { (error) in
                print("Error fetching videos of tv show")
                self.delegate?.showNoVideosLabel()
        }
    }
    
    private func createURLOfVideosForTVShow(videosInfo: [VideoForItem]) -> [String] {
        var urlsOfVideo: [String] = [String]()
        for videoInfo in videosInfo {
            if let site = videoInfo.site, site.lowercased() == "youtube", let key = videoInfo.key {
                let url = "https://www.youtube.com/embed/" + key + "?list=foo&autoplay=0"
                urlsOfVideo.append(url)
            }
        }
        return urlsOfVideo
    }
    
    func collectionViewCellForItemAtIndexPath(request: TVShowDetailViewControllerModel.CollectionViewCellForItemAtIndexPath.Request) -> TVShowDetailViewControllerModel.CollectionViewCellForItemAtIndexPath.Response {
        
        let cell = request.collectionView.dequeueReusableCell(withReuseIdentifier: request.collectionViewCellIdentifier, for: request.indexPath) as! VideoCollectionViewCell
        let request = VideoCollectionViewCellModel.LoadVideo.Request.init(videoURL: self.videosURLForTVShow[request.indexPath.row])
        cell.loadVideo(request: request)
        return TVShowDetailViewControllerModel.CollectionViewCellForItemAtIndexPath.Response.init(collectionViewCell: cell)
    }
    
    func collectionViewNumberOfItemsInSection() -> TVShowDetailViewControllerModel.CollectionViewNumberOfItemsInSection.Response {
        return TVShowDetailViewControllerModel.CollectionViewNumberOfItemsInSection.Response.init(numberOfItems: self.videosURLForTVShow.count)
    }
    
}
