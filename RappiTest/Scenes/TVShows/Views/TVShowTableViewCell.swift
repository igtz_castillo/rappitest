//
//  TVShowTableViewCell.swift
//  RappiTest
//
//  Created by Israel Gtz on 8/13/19.
//  Copyright © 2019 Rohkeus Systems. All rights reserved.
//

import Foundation
import UIKit

class TVShowTableViewCell: UITableViewCell {
    
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var averageVoteLabel: UILabel!
    @IBOutlet weak var overviewLabel: UILabel!
    @IBOutlet weak var posterImageView: UIImageViewForDownloadImages!
    
    func setAppearance(request: TVShowTableViewCellModel.SetAppearance.Request) {
        self.posterImageView.downloadAndCacheImage(request.tvShowData.poster_path ?? "", contentMode: .scaleAspectFill, sizeToCrop: nil, actionsToDoWhenError: {
            DispatchQueue.main.async {
                self.posterImageView.image = #imageLiteral(resourceName: "movieDefaultImage")
            }
        }, actionsWhenSucceed: nil) {}
        self.nameLabel.text = request.tvShowData.name
        self.averageVoteLabel.text = ("\(request.tvShowData.vote_average)")
        self.overviewLabel.text = request.tvShowData.overview
    }
    
}
