//
//  SearchingTVShowsViewControllerViewModel.swift
//  RappiTest
//
//  Created by Israel Gtz on 8/15/19.
//  Copyright © 2019 Rohkeus Systems. All rights reserved.
//

import Foundation
import UIKit
import PromiseKit

protocol SearchingTVShowsViewControllerViewModelDelegate: PresentLoaderLogic {
    func reloadTable()
    func pushViewController(viewController: UIViewController)
}

class SearchingTVShowsViewControllerViewModel {
    var allTVShows: [TVShow] = [TVShow]() {
        didSet {
            self.delegate?.reloadTable()
        }
    }
    var delegate: SearchingTVShowsViewControllerViewModelDelegate?
    
    func getTVShowsData(searchingText: String) {
        self.delegate?.showLoader(request: PresentLoaderLogicModel.ShowLoader.Request.init(message: .fetchingSearchedTVShows))
        let params: [String: Any] = ["search": searchingText]
        NetworkManager.shared.doRequest(type: .getConfigurationPaths, parameters: nil).then { (configurationResponse) -> Promise<Any> in
            NetworkManager.shared.doRequest(type: .getSearchingTVShows, parameters: params)}.done { (responseFromServer) in
                guard let searchingTVShowsResponse = responseFromServer as? SearchingTVShowsResponse, let tvShows = searchingTVShowsResponse.results else {
                    self.delegate?.hideLoader()
                    self.delegate?.showMessage(request: PresentLoaderLogicModel.ShowMessage.Request.init(message: Messages.errorParsingData.rawValue, title: nil))
                    return
                }
            self.allTVShows = tvShows
            self.delegate?.hideLoader()
        }.catch { (error) in
            self.delegate?.hideLoader()
            guard let errorType = error as? ErrorType.RequestServer else {
                self.delegate?.showMessage(request: PresentLoaderLogicModel.ShowMessage.Request.init(message: Messages.errorFetchingSearchedMoviesOrTVShows.rawValue, title: nil))
                return
            }
            switch errorType {
            case ErrorType.RequestServer.noNetworkDetected:
                self.delegate?.showMessage(request: PresentLoaderLogicModel.ShowMessage.Request.init(message: Messages.errorNoNetworkDetected.rawValue, title: nil))
            default:
                self.delegate?.showMessage(request: PresentLoaderLogicModel.ShowMessage.Request.init(message: Messages.errorFetchingSearchedMoviesOrTVShows.rawValue, title: nil))
            }
        }
    }
    
    func tableViewDidSelectRowAt(request: SearchingTVShowsViewControllerModel.TableViewDidSelectRowAt.Request) {
        let tvShowData = self.allTVShows[request.indexPath.row]
        self.delegate?.pushViewController(viewController: self.instanceOfMovieDetailViewController(tvShowData: tvShowData))
    }
    
    private func instanceOfMovieDetailViewController(tvShowData: TVShow) -> TVShowDetailViewController {
        let storyboard = UIStoryboard(name: "TVShowDetailStoryboard", bundle: nil)
        let viewController = storyboard.instantiateViewController(withIdentifier: "TVShowDetailViewControllerID") as! TVShowDetailViewController
        viewController.viewModel.tvShowData = tvShowData
        return viewController
    }
    
    func numberOfRowsInSection(request: SearchingTVShowsViewControllerModel.NumberOfRowsInSection.Request) -> SearchingTVShowsViewControllerModel.NumberOfRowsInSection.Response {
        return SearchingTVShowsViewControllerModel.NumberOfRowsInSection.Response.init(numberOfElements: self.allTVShows.count)
        
    }
    
    func tableViewCellForRowAt(request: SearchingTVShowsViewControllerModel.TableViewCellForRowAt.Request) -> SearchingTVShowsViewControllerModel.TableViewCellForRowAt.Response {
        let cell = request.tableView.dequeueReusableCell(withIdentifier: request.cellIdentifier, for: request.indexPath) as! TVShowTableViewCell
        let setAppearanceCellRequest = TVShowTableViewCellModel.SetAppearance.Request.init(tvShowData: self.allTVShows[request.indexPath.row])
        cell.setAppearance(request: setAppearanceCellRequest)
        return SearchingTVShowsViewControllerModel.TableViewCellForRowAt.Response.init(cell: cell)
    }
    
    func searchBarSearchButtonTapped(request: SearchingTVShowsViewControllerModel.SearchBarSearchButtonTapped.Request) {
        self.getTVShowsData(searchingText: request.searchingText)
    }
    
    func deleteAllTVShows() {
        self.allTVShows.removeAll()
        self.allTVShows = [TVShow]()
    }
}
