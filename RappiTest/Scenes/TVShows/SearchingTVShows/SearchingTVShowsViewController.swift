//
//  SearchingTVShowsViewController.swift
//  RappiTest
//
//  Created by Israel Gtz on 8/15/19.
//  Copyright © 2019 Rohkeus Systems. All rights reserved.
//

import Foundation
import UIKit

class SearchingTVShowsViewController: UIViewController, PresentLoaderLogic {
    
    @IBOutlet weak var mainTableView: UITableView!
    @IBOutlet weak var searchBar: UISearchBar!
    
    lazy var viewModel: SearchingTVShowsViewControllerViewModel = {
        return SearchingTVShowsViewControllerViewModel()
    }()
    
    private let kCellIdentifier = "cellIdentifier"
    
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
        self.setUpViewModel()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.setUpViewModel()
    }
    
    private func setUpViewModel() {
        self.viewModel.delegate = self
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setMainTableView()
        self.setSearchBar()
        self.hideKeyboardWhenTappedAround()
    }
    
    private func setMainTableView() {
        self.mainTableView.delegate = self
        self.mainTableView.dataSource = self
        self.mainTableView.register(UINib.init(nibName: "TVShowTableViewCell", bundle: nil), forCellReuseIdentifier: self.kCellIdentifier)
        self.mainTableView.tableHeaderView = self.searchBar
    }
    
    private func setSearchBar() {
        self.searchBar.delegate = self
    }
    
}

extension SearchingTVShowsViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let request = SearchingTVShowsViewControllerModel.TableViewDidSelectRowAt.Request.init(indexPath: indexPath)
        self.viewModel.tableViewDidSelectRowAt(request: request)
    }
    
}

extension SearchingTVShowsViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let request = SearchingTVShowsViewControllerModel.NumberOfRowsInSection.Request.init(section: section)
        return self.viewModel.numberOfRowsInSection(request: request).numberOfElements
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let request = SearchingTVShowsViewControllerModel.TableViewCellForRowAt.Request.init(tableView: tableView, indexPath: indexPath, cellIdentifier: self.kCellIdentifier)
        return self.viewModel.tableViewCellForRowAt(request: request).cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 250.0
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 250.0
    }
    
}

extension SearchingTVShowsViewController: UISearchBarDelegate {
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        guard let searchingText = searchBar.text, searchingText != "" else {
            return
        }
        let request = SearchingTVShowsViewControllerModel.SearchBarSearchButtonTapped.Request.init(searchingText: searchingText)
        self.viewModel.searchBarSearchButtonTapped(request: request)
        self.dismissKeyboard()
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if searchText == "" {
            self.dismissKeyboard()
            self.viewModel.deleteAllTVShows()
        }
    }
}

extension SearchingTVShowsViewController: SearchingTVShowsViewControllerViewModelDelegate {
    func reloadTable() {
        self.mainTableView.reloadData()
    }
    
    func pushViewController(viewController: UIViewController) {
        self.navigationController?.pushViewController(viewController, animated: true)
    }
}
