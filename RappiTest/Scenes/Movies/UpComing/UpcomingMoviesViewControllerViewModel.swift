//
//  UpcomingMoviesViewControllerViewModel.swift
//  RappiTest
//
//  Created by Israel Gtz on 8/13/19.
//  Copyright © 2019 Rohkeus Systems. All rights reserved.
//

import Foundation
import UIKit
import PromiseKit

protocol UpcomingMoviesViewControllerViewModelDelegate: PresentLoaderLogic {
    func reloadTable()
    func endRefreshingTableView()
    func pushViewController(viewController: UIViewController)
}

class UpcomingMoviesViewControllerViewModel {
    
    var allMovies: [Movie] = [Movie]() {
        didSet {
            self.delegate?.reloadTable()
            self.delegate?.endRefreshingTableView()
        }
    }
    var filteredMovies: [Movie] = [Movie]()
    var isSearching: Bool = false
    var delegate: UpcomingMoviesViewControllerViewModelDelegate?
    
    func getMoviesData() {
        self.delegate?.showLoader(request: PresentLoaderLogicModel.ShowLoader.Request.init(message: .fetchingUpcomingMovies))
        NetworkManager.shared.doRequest(type: .getConfigurationPaths, parameters: nil).then { (configurationResponse) -> Promise<Any> in
            NetworkManager.shared.doRequest(type: .getUpcomingMovies, parameters: nil)
        }.done { (responseFromServer) in
            guard let upcomingMoviesResponse = responseFromServer as? UpcomingMoviesResponse, let movies = upcomingMoviesResponse.results else {
                self.delegate?.hideLoader()
                self.delegate?.showMessage(request: PresentLoaderLogicModel.ShowMessage.Request.init(message: Messages.errorParsingData.rawValue, title: nil))
                self.delegate?.endRefreshingTableView()
                return
            }
            self.allMovies = movies
            self.delegate?.hideLoader()
        }.catch { (error) in
            let responseFromCoreData = CoreDataManager.getMoviesOfSomeType(request: CoreDataManagerModel.GetMoviesOfSomeType.Request.init(type: .upcoming))
            if responseFromCoreData.error != nil {
                self.delegate?.hideLoader()
                self.delegate?.showMessage(request: PresentLoaderLogicModel.ShowMessage.Request.init(message: Messages.noLocalStorageData.rawValue, title: nil))
                self.delegate?.endRefreshingTableView()
                return
            } else {
                self.allMovies = responseFromCoreData.movies
                self.delegate?.hideLoader()
                if responseFromCoreData.movies.count == 0 {
                    self.delegate?.showMessage(request: PresentLoaderLogicModel.ShowMessage.Request.init(message: Messages.noLocalStorageData.rawValue, title: nil))
                }
            }
        }
    }
    
    func tableViewDidSelectRowAt(request: UpcomingMoviesViewControllerModel.TableViewDidSelectRowAt.Request) {
        var movieData: Movie
        if self.isSearching {
            movieData = self.filteredMovies[request.indexPath.row]
        } else {
            movieData = self.allMovies[request.indexPath.row]
        }
        self.delegate?.pushViewController(viewController: self.instanceOfMovieDetailViewController(movieData: movieData))
    }
    
    private func instanceOfMovieDetailViewController(movieData: Movie) -> MovieDetailViewController {
        let storyboard = UIStoryboard(name: "MovieDetailStoryboard", bundle: nil)
        let viewController = storyboard.instantiateViewController(withIdentifier: "MovieDetailViewControllerID") as! MovieDetailViewController
        viewController.viewModel.movieData = movieData
        return viewController
    }
    
    func numberOfRowsInSection(request: UpcomingMoviesViewControllerModel.NumberOfRowsInSection.Request) -> UpcomingMoviesViewControllerModel.NumberOfRowsInSection.Response {
        if self.isSearching {
            return UpcomingMoviesViewControllerModel.NumberOfRowsInSection.Response.init(numberOfElements: self.filteredMovies.count)
        } else {
            return UpcomingMoviesViewControllerModel.NumberOfRowsInSection.Response.init(numberOfElements: self.allMovies.count)
        }
    }
    
    func tableViewCellForRowAt(request: UpcomingMoviesViewControllerModel.TableViewCellForRowAt.Request) -> UpcomingMoviesViewControllerModel.TableViewCellForRowAt.Response {
        let cell = request.tableView.dequeueReusableCell(withIdentifier: request.cellIdentifier, for: request.indexPath) as! MovieTableViewCell
        let setAppearanceCellRequest = MovieTableViewCellModel.SetAppearance.Request.init(movieData: self.isSearching ? self.filteredMovies[request.indexPath.row] : self.allMovies[request.indexPath.row])
        cell.setAppearance(request: setAppearanceCellRequest)
        return UpcomingMoviesViewControllerModel.TableViewCellForRowAt.Response.init(cell: cell)
    }
    
    func searchBarTextDidChange(request: UpcomingMoviesViewControllerModel.SearchBarTextDidChange.Request) {
        if request.searchingText == "" {
            self.isSearching = false
        } else {
            self.isSearching = true
        }
        self.filterElements(searchingText: request.searchingText)
    }
    
    private func filterElements(searchingText: String) {
        if searchingText == "" {
            self.filteredMovies = self.allMovies
        } else {
            var searchResults = self.allMovies
            let strippedString = searchingText.trimmingCharacters(in: .whitespaces)
            var searchItems: [String] = Array()
            if strippedString.count > 0 {
                searchItems = strippedString.components(separatedBy: " ")
            }
            var andMatchPredictaes: [NSCompoundPredicate] = Array()
            for searchString in searchItems {
                var searchItemsPredicate: [NSPredicate] = Array()
                
                let lhs: NSExpression = NSExpression(forKeyPath: "title")
                let rhs: NSExpression = NSExpression(forConstantValue: searchString)
                let finalPredicate: NSComparisonPredicate = NSComparisonPredicate(leftExpression: lhs,
                                                                                  rightExpression: rhs,
                                                                                  modifier: .direct,
                                                                                  type: .contains,
                                                                                  options: NSComparisonPredicate.Options.caseInsensitive)
                searchItemsPredicate.append(finalPredicate)
                let orMatchPredicates: NSCompoundPredicate = NSCompoundPredicate(orPredicateWithSubpredicates: searchItemsPredicate)
                andMatchPredictaes.append(orMatchPredicates)
            }
            
            let finalCompoundPredicate = NSCompoundPredicate(andPredicateWithSubpredicates: andMatchPredictaes)
            let nsArraySearchResults = searchResults as NSArray
            searchResults = (nsArraySearchResults.filtered(using: finalCompoundPredicate) as? [Movie]) ?? Array()
            self.filteredMovies = searchResults
        }
        self.delegate?.reloadTable()
    }
    
    
}

