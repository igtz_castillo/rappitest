//
//  SearchingMoviesViewControllerViewModel.swift
//  RappiTest
//
//  Created by Israel Gtz on 8/14/19.
//  Copyright © 2019 Rohkeus Systems. All rights reserved.
//

import Foundation
import UIKit
import PromiseKit

protocol SearchingMoviesViewControllerViewModelDelegate: PresentLoaderLogic {
    func reloadTable()
    func pushViewController(viewController: UIViewController)
}

class SearchingMoviesViewControllerViewModel {
    var allMovies: [Movie] = [Movie]() {
        didSet {
            self.delegate?.reloadTable()
        }
    }
    var delegate: SearchingMoviesViewControllerViewModelDelegate?
    
    private func getMoviesData(searchingText: String) {
        self.delegate?.showLoader(request: PresentLoaderLogicModel.ShowLoader.Request.init(message: .fetchingSearchedMovies))
        let params: [String: Any] = ["search": searchingText]
        NetworkManager.shared.doRequest(type: .getConfigurationPaths, parameters: nil).then { (configurationResponse) -> Promise<Any> in
            NetworkManager.shared.doRequest(type: .getSearchingMovies, parameters: params)
            }.done { (responseFromServer) in
                guard let searchingMoviesResponse = responseFromServer as? SearchingMoviesResponse, let movies = searchingMoviesResponse.results else {
                    self.delegate?.hideLoader()
                    self.delegate?.showMessage(request: PresentLoaderLogicModel.ShowMessage.Request.init(message: Messages.errorParsingData.rawValue, title: nil))
                    return
                }
                self.allMovies = movies
                self.delegate?.hideLoader()
        }.catch { (error) in
            self.delegate?.hideLoader()
            guard let errorType = error as? ErrorType.RequestServer else {
                self.delegate?.showMessage(request: PresentLoaderLogicModel.ShowMessage.Request.init(message: Messages.errorFetchingSearchedMoviesOrTVShows.rawValue, title: nil))
                return
            }
            switch errorType {
            case ErrorType.RequestServer.noNetworkDetected:
                self.delegate?.showMessage(request: PresentLoaderLogicModel.ShowMessage.Request.init(message: Messages.errorNoNetworkDetected.rawValue, title: nil))
            default:
                self.delegate?.showMessage(request: PresentLoaderLogicModel.ShowMessage.Request.init(message: Messages.errorFetchingSearchedMoviesOrTVShows.rawValue, title: nil))
            }
        }
    }
    
    func tableViewDidSelectRowAt(request: SearchingMoviesViewControllerModel.TableViewDidSelectRowAt.Request) {
        let movieData = self.allMovies[request.indexPath.row]
        self.delegate?.pushViewController(viewController: self.instanceOfMovieDetailViewController(movieData: movieData))
    }
    
    private func instanceOfMovieDetailViewController(movieData: Movie) -> MovieDetailViewController {
        let storyboard = UIStoryboard(name: "MovieDetailStoryboard", bundle: nil)
        let viewController = storyboard.instantiateViewController(withIdentifier: "MovieDetailViewControllerID") as! MovieDetailViewController
        viewController.viewModel.movieData = movieData
        return viewController
    }
    
    func numberOfRowsInSection(request: SearchingMoviesViewControllerModel.NumberOfRowsInSection.Request) -> SearchingMoviesViewControllerModel.NumberOfRowsInSection.Response {
        return SearchingMoviesViewControllerModel.NumberOfRowsInSection.Response.init(numberOfElements: self.allMovies.count)
    }
    
    func tableViewCellForRowAt(request: SearchingMoviesViewControllerModel.TableViewCellForRowAt.Request) -> SearchingMoviesViewControllerModel.TableViewCellForRowAt.Response {
        let cell = request.tableView.dequeueReusableCell(withIdentifier: request.cellIdentifier, for: request.indexPath) as! MovieTableViewCell
        let setAppearanceCellRequest = MovieTableViewCellModel.SetAppearance.Request.init(movieData: self.allMovies[request.indexPath.row])
        cell.setAppearance(request: setAppearanceCellRequest)
        return SearchingMoviesViewControllerModel.TableViewCellForRowAt.Response.init(cell: cell)
    }
    

    
    func searchBarSearchButtonTapped(request: SearchingMoviesViewControllerModel.SearchBarSearchButtonTapped.Request) {
        self.getMoviesData(searchingText: request.searchingText)
    }
    
    func deleteAllMovies() {
        self.allMovies.removeAll()
        self.allMovies = [Movie]()
    }
    
}
