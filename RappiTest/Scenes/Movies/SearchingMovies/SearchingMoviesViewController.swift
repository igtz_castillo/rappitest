//
//  SearchingMoviesViewController.swift
//  RappiTest
//
//  Created by Israel Gtz on 8/14/19.
//  Copyright © 2019 Rohkeus Systems. All rights reserved.
//

import Foundation
import UIKit

class SearchingMoviesViewController: UIViewController, PresentLoaderLogic {
    
    @IBOutlet weak var mainTableView: UITableView!
    @IBOutlet weak var searchBar: UISearchBar!
    
    lazy var viewModel: SearchingMoviesViewControllerViewModel = {
        return SearchingMoviesViewControllerViewModel()
    }()
    
    private let kCellIdentifier = "cellIdentifier"
    
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
        self.setUpViewModel()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.setUpViewModel()
    }
    
    private func setUpViewModel() {
        self.viewModel.delegate = self
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setMainTableView()
        self.setSearchBar()
        self.hideKeyboardWhenTappedAround()
    }
    
    private func setMainTableView() {
        self.mainTableView.delegate = self
        self.mainTableView.dataSource = self
        self.mainTableView.register(UINib.init(nibName: "MovieTableViewCell", bundle: nil), forCellReuseIdentifier: self.kCellIdentifier)
        self.mainTableView.tableHeaderView = self.searchBar
    }
    
    private func setSearchBar() {
        self.searchBar.delegate = self
    }
}

extension SearchingMoviesViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let request = SearchingMoviesViewControllerModel.TableViewDidSelectRowAt.Request.init(indexPath: indexPath)
        self.viewModel.tableViewDidSelectRowAt(request: request)
    }
}

extension SearchingMoviesViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let request = SearchingMoviesViewControllerModel.NumberOfRowsInSection.Request.init(section: section)
        return self.viewModel.numberOfRowsInSection(request: request).numberOfElements
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let request = SearchingMoviesViewControllerModel.TableViewCellForRowAt.Request.init(tableView: tableView, indexPath: indexPath, cellIdentifier: self.kCellIdentifier)
        return self.viewModel.tableViewCellForRowAt(request: request).cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 250.0
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 250.0
    }
}

extension SearchingMoviesViewController: UISearchBarDelegate {    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        guard let searchingText = searchBar.text, searchingText != "" else {
            return
        }
        let request = SearchingMoviesViewControllerModel.SearchBarSearchButtonTapped.Request.init(searchingText: searchingText)
        self.viewModel.searchBarSearchButtonTapped(request: request)
        self.dismissKeyboard()
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if searchText == "" {
            self.dismissKeyboard()
            self.viewModel.deleteAllMovies()
        }
    }
    
}

extension SearchingMoviesViewController: SearchingMoviesViewControllerViewModelDelegate {
    func reloadTable() {
        self.mainTableView.reloadData()
    }
    
    func pushViewController(viewController: UIViewController) {
        self.navigationController?.pushViewController(viewController, animated: true)
    }
}
