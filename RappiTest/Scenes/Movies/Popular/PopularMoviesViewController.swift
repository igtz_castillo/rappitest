//
//  PopularMoviesViewController.swift
//  RappiTest
//
//  Created by Israel Gtz on 8/11/19.
//  Copyright © 2019 Rohkeus Systems. All rights reserved.
//

import Foundation
import UIKit

class PopularMoviesViewController: UIViewController, PresentLoaderLogic {
    
    @IBOutlet weak var mainTableView: UITableView!
    @IBOutlet weak var searchBar: UISearchBar!
    
    lazy var viewModel: PopularMoviesViewControllerViewModel = {
       return PopularMoviesViewControllerViewModel()
    }()
    
    private let kCellIdentifier = "cellIdentifier"
    
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
        self.setUpViewModel()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.setUpViewModel()
    }

    private func setUpViewModel() {
        self.viewModel.delegate = self
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setMainTableView()
        self.setSearchBar()
        self.hideKeyboardWhenTappedAround()
        self.getMoviesData()
    }
    
    private func setMainTableView() {
        self.mainTableView.delegate = self
        self.mainTableView.dataSource = self
        self.mainTableView.register(UINib.init(nibName: "MovieTableViewCell", bundle: nil), forCellReuseIdentifier: self.kCellIdentifier)
        self.mainTableView.tableHeaderView = self.searchBar
        self.addRefreshControl()
    }
    
    private func addRefreshControl() {
        let refreshControl = UIRefreshControl.init()
        refreshControl.addTarget(self, action: #selector(getMoviesData), for: .valueChanged)
        self.mainTableView.refreshControl = refreshControl
    }
    
    private func setSearchBar() {
        self.searchBar.delegate = self
    }
    
    @objc private func getMoviesData() {
        self.viewModel.getMoviesData()
    }
    
}

extension PopularMoviesViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let request = PopularMoviesViewControllerModel.TableViewDidSelectRowAt.Request.init(indexPath: indexPath)
        self.viewModel.tableViewDidSelectRowAt(request: request)
    }
}

extension PopularMoviesViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let request = PopularMoviesViewControllerModel.NumberOfRowsInSection.Request.init(section: section)
        return self.viewModel.numberOfRowsInSection(request: request).numberOfElements
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let request = PopularMoviesViewControllerModel.TableViewCellForRowAt.Request.init(tableView: tableView, indexPath: indexPath, cellIdentifier: self.kCellIdentifier)
        return self.viewModel.tableViewCellForRowAt(request: request).cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 250.0
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 250.0
    }
}

extension PopularMoviesViewController: UISearchBarDelegate {
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        let request = PopularMoviesViewControllerModel.SearchBarTextDidChange.Request.init(searchingText: searchText)
        self.viewModel.searchBarTextDidChange(request: request)
    }
}

extension PopularMoviesViewController: PopularMoviesViewControllerViewModelDelegate {
    func reloadTable() {
        self.mainTableView.reloadData()
    }
    
    func endRefreshingTableView() {
        self.mainTableView.refreshControl?.endRefreshing()
    }
    
    func pushViewController(viewController: UIViewController) {
        self.navigationController?.pushViewController(viewController, animated: true)
    }
}
