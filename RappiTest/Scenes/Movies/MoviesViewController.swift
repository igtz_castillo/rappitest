//
//  MoviesViewController.swift
//  RappiTest
//
//  Created by Israel Gtz on 8/9/19.
//  Copyright © 2019 Rohkeus Systems. All rights reserved.
//

import Foundation
import UIKit
import PromiseKit
import CoreData

class MoviesViewController: UIViewController {
    
    @IBOutlet weak var segmentedControl: UISegmentedControl!
    @IBOutlet weak var childViewControllerContainerView: UIView!
    
    lazy var viewModel: MoviesViewControllerViewModel = {
       return MoviesViewControllerViewModel()
    }()
    
    lazy var popularMoviesViewController: PopularMoviesViewController = {
        let storyboard = UIStoryboard(name: "MoviesStoryboard", bundle: nil)
        var viewController = storyboard.instantiateViewController(withIdentifier: "PopularMoviesViewControllerID") as! PopularMoviesViewController
        return viewController
    }()
    
    lazy var topRtdMoviesViewController: TopRatedMoviesViewController = {
        let storyboard = UIStoryboard(name: "MoviesStoryboard", bundle: nil)
        var viewController = storyboard.instantiateViewController(withIdentifier: "TopRatedMoviesViewControllerID") as! TopRatedMoviesViewController
        return viewController
    }()
    
    lazy var upComingMoviesViewController: UpcomingMoviesViewController = {
        let storyboard = UIStoryboard(name: "MoviesStoryboard", bundle: nil)
        var viewController = storyboard.instantiateViewController(withIdentifier: "UpcomingMoviesViewControllerID") as! UpcomingMoviesViewController
        return viewController
    }()

    lazy var searchingMoviesViewController: SearchingMoviesViewController = {
        let storyboard = UIStoryboard(name: "MoviesStoryboard", bundle: nil)
        var viewController = storyboard.instantiateViewController(withIdentifier: "SearchingMoviesViewControllerID") as! SearchingMoviesViewController
        return viewController
    }()
    
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
        self.setUpViewModel()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.setUpViewModel()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setSegmentedControl()
        self.hideKeyboardWhenTappedAround()
    }
    
    private func setUpViewModel() {
        
    }
    
    private func setSegmentedControl() {
        self.segmentedControl.addTarget(self, action: #selector(selectionDidChange), for: .valueChanged)
        self.segmentedControl.selectedSegmentIndex = 0
        self.segmentedControl.sendActions(for: .valueChanged)
    }
    
    @objc func selectionDidChange() {
        if self.viewModel.previousSegmentedControlIndexSelected == 0 {
            if self.segmentedControl.selectedSegmentIndex == 1 {
                self.pushNavigationItemTitle(nameOfScreen: .topRatedMovies, direction: .fromRight)
                self.viewModel.changeViewController(add: self.topRtdMoviesViewController, remove: self.popularMoviesViewController, to: self, in: self.childViewControllerContainerView)
            } else
            if self.segmentedControl.selectedSegmentIndex == 2 {
                self.pushNavigationItemTitle(nameOfScreen: .upcomingMovies, direction: .fromRight)
                self.viewModel.changeViewController(add: self.upComingMoviesViewController, remove: self.popularMoviesViewController, to: self, in: self.childViewControllerContainerView)
            } else
            if self.segmentedControl.selectedSegmentIndex == 3 {
                self.pushNavigationItemTitle(nameOfScreen: .searchingMovies, direction: .fromRight)
                self.viewModel.changeViewController(add: self.searchingMoviesViewController, remove: self.popularMoviesViewController, to: self, in: self.childViewControllerContainerView)
            }
        } else
        if self.viewModel.previousSegmentedControlIndexSelected == 1 {
            if self.segmentedControl.selectedSegmentIndex == 0 {
                self.pushNavigationItemTitle(nameOfScreen: .popularMovies, direction: .fromLeft)
                self.viewModel.changeViewController(add: self.popularMoviesViewController, remove: self.topRtdMoviesViewController, to: self, in: self.childViewControllerContainerView)
            } else
            if self.segmentedControl.selectedSegmentIndex == 2 {
                self.pushNavigationItemTitle(nameOfScreen: .upcomingMovies, direction: .fromRight)
                self.viewModel.changeViewController(add: self.upComingMoviesViewController, remove: self.topRtdMoviesViewController, to: self, in: self.childViewControllerContainerView)
            } else
            if self.segmentedControl.selectedSegmentIndex == 3 {
                self.pushNavigationItemTitle(nameOfScreen: .searchingMovies, direction: .fromRight)
                self.viewModel.changeViewController(add: self.searchingMoviesViewController, remove: self.topRtdMoviesViewController, to: self, in: self.childViewControllerContainerView)
            }
        } else
        if self.viewModel.previousSegmentedControlIndexSelected == 2 {
            if self.segmentedControl.selectedSegmentIndex == 0 {
                self.pushNavigationItemTitle(nameOfScreen: .popularMovies, direction: .fromLeft)
                self.viewModel.changeViewController(add: self.popularMoviesViewController, remove: self.upComingMoviesViewController, to: self, in: self.childViewControllerContainerView)
            } else
            if self.segmentedControl.selectedSegmentIndex == 1 {
                self.pushNavigationItemTitle(nameOfScreen: .topRatedMovies, direction: .fromLeft)
                self.viewModel.changeViewController(add: self.topRtdMoviesViewController, remove: self.upComingMoviesViewController, to: self, in: self.childViewControllerContainerView)
            } else
            if self.segmentedControl.selectedSegmentIndex == 3 {
                self.pushNavigationItemTitle(nameOfScreen: .searchingMovies, direction: .fromRight)
                self.viewModel.changeViewController(add: self.searchingMoviesViewController, remove: self.upComingMoviesViewController, to: self, in: self.childViewControllerContainerView)
            }
        } else
        if self.viewModel.previousSegmentedControlIndexSelected == 3 {
            if self.segmentedControl.selectedSegmentIndex == 0 {
                self.pushNavigationItemTitle(nameOfScreen: .popularMovies, direction: .fromLeft)
                self.viewModel.changeViewController(add: self.popularMoviesViewController, remove: self.searchingMoviesViewController, to: self, in: self.childViewControllerContainerView)
            } else
            if self.segmentedControl.selectedSegmentIndex == 1 {
                self.pushNavigationItemTitle(nameOfScreen: .topRatedMovies, direction: .fromLeft)
                self.viewModel.changeViewController(add: self.topRtdMoviesViewController, remove: self.searchingMoviesViewController, to: self, in: self.childViewControllerContainerView)
            } else
            if self.segmentedControl.selectedSegmentIndex == 2 {
                self.pushNavigationItemTitle(nameOfScreen: .upcomingMovies, direction: .fromLeft)
                self.viewModel.changeViewController(add: self.upComingMoviesViewController, remove: self.searchingMoviesViewController, to: self, in: self.childViewControllerContainerView)
            }
        }
        self.viewModel.previousSegmentedControlIndexSelected = self.segmentedControl.selectedSegmentIndex
    }
    
    private func pushNavigationItemTitle(nameOfScreen: NameOfMoviesScreens, direction: CATransitionSubtype) {
        let pushTextAnimation = CATransition()
        pushTextAnimation.duration = 0.15
        pushTextAnimation.type = .push
        pushTextAnimation.subtype = direction
        self.navigationController?.navigationBar.layer.add(pushTextAnimation, forKey: "pushText")
        self.navigationItem.title = nameOfScreen.rawValue
    }
    
}
