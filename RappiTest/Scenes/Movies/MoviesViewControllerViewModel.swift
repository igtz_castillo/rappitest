//
//  MoviesViewControllerViewModel.swift
//  RappiTest
//
//  Created by Israel Gtz on 8/9/19.
//  Copyright © 2019 Rohkeus Systems. All rights reserved.
//

import Foundation
import UIKit

class MoviesViewControllerViewModel {
    
    var previousSegmentedControlIndexSelected: Int = 1
    
    internal func changeViewController(add addThisViewController: UIViewController, remove removeThisViewController: UIViewController, to parentViewController: UIViewController, in inThisView: UIView) {
        
        self.remove(asChildViewController: removeThisViewController)
        self.add(asChildViewController: addThisViewController, parentViewController: parentViewController, inThisView: inThisView)
    }
    
    private func add(asChildViewController viewController: UIViewController, parentViewController: UIViewController, inThisView: UIView) {
        parentViewController.addChild(viewController)
        inThisView.addSubview(viewController.view)
        viewController.view.frame = inThisView.bounds
        viewController.view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        
        // Notify Child View Controller
        viewController.didMove(toParent: parentViewController)
    }
    
    private func remove(asChildViewController viewController: UIViewController) {
        viewController.willMove(toParent: nil)
        viewController.view.removeFromSuperview()
        viewController.removeFromParent()
    }
    
    
}
