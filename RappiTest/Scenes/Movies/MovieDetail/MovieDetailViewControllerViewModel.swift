//
//  MovieDetailViewControllerViewModel.swift
//  RappiTest
//
//  Created by Israel Gtz on 8/12/19.
//  Copyright © 2019 Rohkeus Systems. All rights reserved.
//

import Foundation

protocol MovieDetailViewControllerViewModelDelegate {
    func reloadCollectionView()
    func showNoVideosLabel()
    func animateArrowForMoreVideos()
}

class MovieDetailViewControllerViewModel {
    
    var movieData: Movie? = nil
    var videosURLForMovie: [String] = [String]() {
        didSet {
            self.delegate?.reloadCollectionView()
            if self.videosURLForMovie.count == 0 {
                self.delegate?.showNoVideosLabel()
            } else
                if self.videosURLForMovie.count > 1 {
                    self.delegate?.animateArrowForMoreVideos()
            }
        }
    }
    var delegate: MovieDetailViewControllerViewModelDelegate?
    
    func getVideosOfMovie() {
        guard let movieData = self.movieData else {
            return
        }
        let movieId = ("\(movieData.id)")
        let params: [String: Any] = ["movieId": movieId]
        NetworkManager.shared.doRequest(type: .getVideosOfMovie, parameters: params).done { (response) in
            guard let videosMovieResponse = response as? MovieAndTVShowsVideosResponse else {
                return
            }
            self.videosURLForMovie = self.createURLOfVideosForMovie(videosInfo: videosMovieResponse.results ?? [VideoForItem]())
        }.catch { (error) in
            print("Error fetching videos of movie")
            self.delegate?.showNoVideosLabel()
        }
    }
    
    private func createURLOfVideosForMovie(videosInfo: [VideoForItem]) -> [String] {
        var urlsOfVideo: [String] = [String]()
        for videoInfo in videosInfo {
            if let site = videoInfo.site, site.lowercased() == "youtube", let key = videoInfo.key {
                let url = "https://www.youtube.com/embed/" + key + "?list=foo&autoplay=0"
                urlsOfVideo.append(url)
            }
        }
        return urlsOfVideo
    }
    
    func collectionViewCellForItemAtIndexPath(request: MovieDetailViewControllerModel.CollectionViewCellForItemAtIndexPath.Request) -> MovieDetailViewControllerModel.CollectionViewCellForItemAtIndexPath.Response {
        
        let cell = request.collectionView.dequeueReusableCell(withReuseIdentifier: request.collectionViewCellIdentifier, for: request.indexPath) as! VideoCollectionViewCell
        let request = VideoCollectionViewCellModel.LoadVideo.Request.init(videoURL: self.videosURLForMovie[request.indexPath.row])
        cell.loadVideo(request: request)
        return MovieDetailViewControllerModel.CollectionViewCellForItemAtIndexPath.Response.init(collectionViewCell: cell)
    }
    
    func collectionViewNumberOfItemsInSection() -> MovieDetailViewControllerModel.CollectionViewNumberOfItemsInSection.Response {
        return MovieDetailViewControllerModel.CollectionViewNumberOfItemsInSection.Response.init(numberOfItems: videosURLForMovie.count)
    }
    
}
