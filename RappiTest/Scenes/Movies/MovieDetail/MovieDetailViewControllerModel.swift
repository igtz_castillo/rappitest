//
//  MovieDetailViewControllerModel.swift
//  RappiTest
//
//  Created by Israel Gtz on 8/12/19.
//  Copyright © 2019 Rohkeus Systems. All rights reserved.
//

import Foundation
import UIKit

enum MovieDetailViewControllerModel {
    
    enum GetVideosOfMovie {
        struct Request {
            var movieId: String
        }
        struct Response {
            var videoURLOfMovie: [String]
        }
    }
    
    enum CollectionViewCellForItemAtIndexPath {
        
        struct Request {
            var collectionView: UICollectionView
            var indexPath: IndexPath
            var collectionViewCellIdentifier: String
        }
        
        struct Response {
            var collectionViewCell: VideoCollectionViewCell
        }
        
    }
    
    enum CollectionViewNumberOfItemsInSection {
        struct Response {
            var numberOfItems: Int
        }
    }
    
    
    
}
