//
//  PopularMovieTableViewCell.swift
//  RappiTest
//
//  Created by Israel Gtz on 8/11/19.
//  Copyright © 2019 Rohkeus Systems. All rights reserved.
//

import Foundation
import UIKit

class MovieTableViewCell: UITableViewCell {
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var voteAverageLabel: UILabel!
    @IBOutlet weak var overviewLabel: UILabel!
    @IBOutlet weak var movieImage: UIImageViewForDownloadImages!
    
    func setAppearance(request: MovieTableViewCellModel.SetAppearance.Request) {
        self.movieImage.downloadAndCacheImage(request.movieData.poster_path ?? "", contentMode: .scaleAspectFill, sizeToCrop: nil, actionsToDoWhenError: {
            DispatchQueue.main.async {
                self.movieImage.image = #imageLiteral(resourceName: "movieDefaultImage")
            }
        }, actionsWhenSucceed: nil) {}
        self.titleLabel.text = request.movieData.title
        self.voteAverageLabel.text = ("\(request.movieData.vote_average)")
        self.overviewLabel.text = request.movieData.overview
    }
    
}
