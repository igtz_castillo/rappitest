//
//  TopRatedMoviesViewControllerModel.swift
//  RappiTest
//
//  Created by Israel Gtz on 8/13/19.
//  Copyright © 2019 Rohkeus Systems. All rights reserved.
//

import Foundation
import UIKit

class TopRatedMoviesViewControllerModel {
    
    enum NumberOfRowsInSection {
        struct Request {
            var section: Int
        }
        
        struct Response {
            var numberOfElements: Int
        }
    }
    
    enum TableViewCellForRowAt {
        struct Request {
            var tableView: UITableView
            var indexPath: IndexPath
            var cellIdentifier: String
        }
        
        struct Response {
            var cell: MovieTableViewCell
        }
    }
    
    enum TableViewDidSelectRowAt {
        struct Request {
            var indexPath: IndexPath
        }
    }
    
    enum SearchBarTextDidChange {
        struct Request {
            var searchingText: String
        }
        
        struct Response {
            var matchedMovies: [Movie]
        }
    }
    
}
