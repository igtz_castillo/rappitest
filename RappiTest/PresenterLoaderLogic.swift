//
//  PresenterLoaderLogic.swift
//  RappiTest
//
//  Created by Israel Gtz on 8/15/19.
//  Copyright © 2019 Rohkeus Systems. All rights reserved.
//

import Foundation
import UIKit

protocol PresentLoaderLogic: class {
    func showLoader(request: PresentLoaderLogicModel.ShowLoader.Request)
    func showMessage(request: PresentLoaderLogicModel.ShowMessage.Request)
    func hideLoader()
}

extension PresentLoaderLogic where Self: UIViewController {
    internal func showLoader(request: PresentLoaderLogicModel.ShowLoader.Request) {
        InterfaceUtil.shared.showLoader(request.message.rawValue)
    }
    
    internal func showMessage(request: PresentLoaderLogicModel.ShowMessage.Request) {
        InterfaceUtil.shared.showMessage(request.title, message: request.message, overViewcontroller: request.overViewController, actionsToDoAfterTapOk: request.actionsToDoAfterTapOk)
    }
    
    internal func hideLoader() {
        InterfaceUtil.shared.hideLoader()
    }
}

class PresentLoaderLogicModel {
    enum ShowLoader {
        struct Request {
            var message: Messages
        }
    }
    
    enum ShowMessage {
        struct Request {
            var message: String?
            var title: String?
            var overViewController: UIViewController?
            var actionsToDoAfterTapOk: (()->())?
            
            init(message: String?, title: String?, overViewController: UIViewController? = nil, actionsToDoAfterTapOk: (()->())? = nil) {
                self.message = message
                self.title = title
                self.overViewController = overViewController
                self.actionsToDoAfterTapOk = actionsToDoAfterTapOk
            }
        }
    }
}
