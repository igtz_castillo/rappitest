//
//  CoreDataManagerModel.swift
//  RappiTest
//
//  Created by Israel Gtz on 8/10/19.
//  Copyright © 2019 Rohkeus Systems. All rights reserved.
//

import Foundation

class CoreDataManagerModel {
    
    enum GetMoviesOfSomeType {
        struct Request {
            var type: TypeOfMovies
        }
        
        struct Response {
            var movies: [Movie]
            var error: Error?
        }
    }
    
    enum GetTVShowsOfSomeType {
        struct Request {
            var type: TypeOfTVShows
        }
        struct Response {
            var tvshows: [TVShow]
            var error: Error?
        }
    }
}
