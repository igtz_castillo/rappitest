//
//  TVShow+CoreDataProperties.swift
//  RappiTest
//
//  Created by Israel Gtz on 8/13/19.
//  Copyright © 2019 Rohkeus Systems. All rights reserved.
//
//

import Foundation
import CoreData


extension TVShow {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<TVShow> {
        return NSFetchRequest<TVShow>(entityName: "TVShow")
    }

    @NSManaged public var backdrop_path: String?
    @NSManaged public var first_air_date: String?
    @NSManaged public var genre_ids: [Int]?
    @NSManaged public var id: Int64
    @NSManaged public var name: String?
    @NSManaged public var origin_country: [String]?
    @NSManaged public var original_language: String?
    @NSManaged public var original_name: String?
    @NSManaged public var overview: String?
    @NSManaged public var popularity: Double
    @NSManaged public var poster_path: String?
    @NSManaged public var type: Int16
    @NSManaged public var vote_average: Double
    @NSManaged public var vote_count: Int64

}
