//
//  TVShow+CoreDataClass.swift
//  RappiTest
//
//  Created by Israel Gtz on 8/13/19.
//  Copyright © 2019 Rohkeus Systems. All rights reserved.
//
//

import Foundation
import CoreData

@objc(TVShow)
public class TVShow: NSManagedObject, Codable {

    enum CodingKeys: String, CodingKey { // declaring keys
        case backdrop_path = "backdrop_path"
        case first_air_date = "first_air_date"
        case genre_ids = "genre_ids"
        case id = "id"
        case name = "name"
        case origin_country = "origin_country"
        case original_language = "original_language"
        case original_name = "original_name"
        case overview = "overview"
        case popularity = "popularity"
        case poster_path = "poster_path"
        case vote_average = "vote_average"
        case vote_count = "vote_count"
    }
    
    public override init(entity: NSEntityDescription, insertInto context: NSManagedObjectContext?) {
        super.init(entity: entity, insertInto: context)
    }
    
    init(entity: NSEntityDescription, context: NSManagedObjectContext, backdrop_path: String?, first_air_date: String?, genre_ids: [Int]?, id: Int64? = 0, name: String?, origin_country: [String]?, original_language: String?, original_name: String?, overview: String?, popularity: Double? = 0.0, poster_path: String?, vote_average: Double? = 0.0, vote_count: Int64? = 0) {
        super.init(entity: entity, insertInto: context)
        self.backdrop_path = backdrop_path
        self.first_air_date = first_air_date
        self.genre_ids = genre_ids
        self.id = id!
        self.name = name
        self.origin_country = origin_country
        self.original_language = original_language
        self.original_name = original_name
        self.overview = overview
        self.popularity = popularity!
        self.poster_path = poster_path
        self.vote_average = vote_average!
        self.vote_count = vote_count!
    }
    
    required convenience public init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self) // defining our (keyed) container
        var backdrop_path: String?
        var first_air_date: String?
        var genre_ids: [Int]?
        var id: Int64 = 0
        var name: String?
        var origin_country: [String]?
        var original_language: String?
        var original_name: String?
        var overview: String?
        var popularity: Double = 0.0
        var poster_path: String?
        var vote_average: Double = 0.0
        var vote_count: Int64 = 0
        
        do {
            backdrop_path = try container.decode(String.self, forKey: .backdrop_path)
        } catch {
            print("No backdrop_path property in json")
        }
        
        do {
            first_air_date = try container.decode(String.self, forKey: .first_air_date)
        } catch {
            print("No first_air_date property in json")
        }
        
        do {
            genre_ids = try container.decode([Int].self, forKey: .genre_ids)
        } catch {
            print("No genre_ids property in json")
        }
        
        do {
            id = try container.decode(Int64.self, forKey: .id)
        } catch {
            print("No id property in json")
        }
        
        do {
            name = try container.decode(String.self, forKey: .name)
        } catch {
            print("No name property in json")
        }
        
        do {
            origin_country = try container.decode([String].self, forKey: .origin_country)
        } catch {
            print("No origin_country property in json")
        }
        
        do {
            original_language = try container.decode(String.self, forKey: .original_language)
        } catch {
            print("No original_language in json")
        }
        
        do {
            original_name = try container.decode(String.self, forKey: .original_name)
        } catch {
            print("No original_name property in json")
        }
        
        do {
            overview = try container.decode(String.self, forKey: .overview)
        } catch {
            print("No overview property in json")
        }
        
        do {
            popularity = try container.decode(Double.self, forKey: .popularity)
        } catch {
            print("No popularity property in json")
        }
        
        do {
            poster_path = try container.decode(String.self, forKey: .poster_path)
        } catch {
            print("No poster_path property in json")
        }
        
        do {
            vote_average = try container.decode(Double.self, forKey: .vote_average)
        } catch {
            print("No average property in json")
        }
        
        do {
            vote_count = try container.decode(Int64.self, forKey: .vote_count)
        } catch {
            print("No vote_count property in json")
        }
        
        guard let context = decoder.userInfo[CodingUserInfoKey.context!] as? NSManagedObjectContext else { throw ErrorType.LocalStorage.errorDecodingToCoreData }
        guard let entity = NSEntityDescription.entity(forEntityName: "TVShow", in: context) else { throw ErrorType.LocalStorage.errorDecodingToCoreData }
        
        self.init(entity: entity, context: context, backdrop_path: backdrop_path, first_air_date: first_air_date, genre_ids: genre_ids, id: id, name: name, origin_country: origin_country, original_language: original_language, original_name: original_name, overview: overview, popularity: popularity, poster_path: poster_path, vote_average: vote_average, vote_count: vote_count)
    }
    
    public func encode(to encoder: Encoder) throws {}
    
}
