//
//  Movie+CoreDataClass.swift
//  RappiTest
//
//  Created by Israel Gtz on 8/9/19.
//  Copyright © 2019 Rohkeus Systems. All rights reserved.
//
//

import Foundation
import CoreData

@objc(Movie)
public class Movie: NSManagedObject, Codable {
    
    enum CodingKeys: String, CodingKey { // declaring keys
        case adult = "adult"
        case backdrop_path = "backdrop_path"
        case genre_ids = "genre_ids"
        case id = "id"
        case original_language = "original_language"
        case original_title = "original_title"
        case overview = "overview"
        case popularity = "popularity"
        case poster_path = "poster_path"
        case release_date = "release_date"
        case title = "title"
        case video = "video"
        case vote_average = "vote_average"
        case vote_count = "vote_count"
        case hasThis = "dates"
    }
    
    public override init(entity: NSEntityDescription, insertInto context: NSManagedObjectContext?) {
        super.init(entity: entity, insertInto: context)
    }
    
    init(entity: NSEntityDescription, context: NSManagedObjectContext, adult: Bool? = false, backdrop_path: String?, genre_ids: [Int]?, id: Int64 = 0, original_language: String?, original_title: String?, overview: String?, popularity: Double? = 0.0, poster_path: String?, release_date: String?, title: String?, video: Bool? = false, vote_average: Double? = 0.0, vote_count: Int64 = 0, hasThisDates: Dates?) {
        super.init(entity: entity, insertInto: context)
        self.adult = adult!
        self.backdrop_path = backdrop_path
        self.genre_ids = genre_ids
        self.id = id
        self.original_language = original_language
        self.original_title = original_title
        self.overview = overview
        self.popularity = popularity!
        self.poster_path = poster_path
        self.release_date = release_date
        self.title = title
        self.video = video!
        self.vote_average = vote_average!
        self.vote_count = vote_count
        self.hasThis = hasThisDates
    }
    
    required convenience public init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self) // defining our (keyed) container
        var adult: Bool? = nil
        var backdrop_path: String? = nil
        var genre_ids: [Int]? = nil
        var id: Int64 = 0
        var original_language: String? = nil
        var original_title: String? = nil
        var overview: String? = nil
        var popularity: Double = 0.0
        var poster_path: String? = nil
        var release_date: String? = nil
        var title: String? = nil
        var video: Bool = false
        var vote_average: Double = 0.0
        var vote_count: Int64 = 0
        var hasThisDates: Dates? = nil
        
        do {
            adult = try container.decode(Bool.self, forKey: .adult)
        } catch {
            print("No adult property in json")
        }
        
        do {
            backdrop_path = try container.decode(String.self, forKey: .backdrop_path)
        } catch {
            print("No backdrop_path property in json")
        }
        
        do {
            genre_ids = try container.decode([Int].self, forKey: .genre_ids)
        } catch {
            print("No genre_ids property in json")
        }
        
        do {
            id = try container.decode(Int64.self, forKey: .id)
        } catch {
            print("No id property in json")
        }
        
        do {
            original_language = try container.decode(String.self, forKey: .original_language)
        } catch {
            print("No original_language in json")
        }
        
        do {
            original_title = try container.decode(String.self, forKey: .original_title)
        } catch {
            print("No original_title property in json")
        }
        
        do {
            overview = try container.decode(String.self, forKey: .overview)
        } catch {
            print("No overview property in json")
        }
        
        do {
            popularity = try container.decode(Double.self, forKey: .popularity)
        } catch {
            print("No popularity property in json")
        }
        
        do {
            poster_path = try container.decode(String.self, forKey: .poster_path)
        } catch {
            print("No poster_path property in json")
        }
        
        do {
            release_date = try container.decode(String.self, forKey: .release_date)
        } catch {
            print("No release_date property in json")
        }
        
        do {
            title = try container.decode(String.self, forKey: .title)
        } catch {
            print("No title property in json")
        }
        
        do {
            video = try container.decode(Bool.self, forKey: .video)
        } catch {
            print("No video property in json")
        }
        
        do {
            vote_average = try container.decode(Double.self, forKey: .vote_average)
        } catch {
            print("No average property in json")
        }
        
        do {
            vote_count = try container.decode(Int64.self, forKey: .vote_count)
        } catch {
            print("No vote_count property in json")
        }
        
        do {
            hasThisDates = try container.decode(Dates?.self, forKey: .hasThis)
        } catch {
//            print("No dates property in json")
        }

        guard let context = decoder.userInfo[CodingUserInfoKey.context!] as? NSManagedObjectContext else { throw ErrorType.LocalStorage.errorDecodingToCoreData }
        guard let entity = NSEntityDescription.entity(forEntityName: "Movie", in: context) else { throw ErrorType.LocalStorage.errorDecodingToCoreData }
        
        self.init(entity: entity, context: context, adult: adult, backdrop_path: backdrop_path, genre_ids: genre_ids, id: id, original_language: original_language, original_title: original_title, overview: overview, popularity: popularity, poster_path: poster_path, release_date: release_date, title: title, video: video, vote_average: vote_average, vote_count: vote_count, hasThisDates: hasThisDates)
    }
    
    public func encode(to encoder: Encoder) throws {}
    
}
