//
//  Dates+CoreDataClass.swift
//  RappiTest
//
//  Created by Israel Gtz on 8/10/19.
//  Copyright © 2019 Rohkeus Systems. All rights reserved.
//
//

import Foundation
import CoreData

@objc(Dates)
public class Dates: NSManagedObject, Codable {
    
    enum CodingKeys: String, CodingKey { // declaring keys
        case maximum = "maximum"
        case minimum = "minimum"
    }
    
    public override init(entity: NSEntityDescription, insertInto context: NSManagedObjectContext?) {
        super.init(entity: entity, insertInto: context)
    }
    
    init(entity: NSEntityDescription, context: NSManagedObjectContext, maximum: String?, minimum: String?) {
        super.init(entity: entity, insertInto: context)
        self.maximum = maximum
        self.minimum = minimum
    }
    
    required convenience public init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self) // defining our (keyed) container
        let maximum: String? = try container.decode(String?.self, forKey: .maximum)
        let minimum: String? = try container.decode(String?.self, forKey: .minimum)
        
        guard let context = decoder.userInfo[CodingUserInfoKey.context!] as? NSManagedObjectContext else { throw ErrorType.LocalStorage.errorDecodingToCoreData }
        guard let entity = NSEntityDescription.entity(forEntityName: "Dates", in: context) else { throw ErrorType.LocalStorage.errorDecodingToCoreData
        }
        
        self.init(entity: entity, context: context, maximum: maximum, minimum: minimum)
    }
    
    public func encode(to encoder: Encoder) throws {}
    
}
