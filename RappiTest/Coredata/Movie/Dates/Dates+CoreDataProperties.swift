//
//  Dates+CoreDataProperties.swift
//  RappiTest
//
//  Created by Israel Gtz on 8/10/19.
//  Copyright © 2019 Rohkeus Systems. All rights reserved.
//
//

import Foundation
import CoreData


extension Dates {
    @nonobjc public class func fetchRequest() -> NSFetchRequest<Dates> {
        return NSFetchRequest<Dates>(entityName: "Dates")
    }
    
    @NSManaged public var maximum: String?
    @NSManaged public var minimum: String?
}
