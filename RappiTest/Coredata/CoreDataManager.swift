//
//  CoreDataManager.swift
//  RappiTest
//
//  Created by Israel Gtz on 8/10/19.
//  Copyright © 2019 Rohkeus Systems. All rights reserved.
//

import Foundation
import UIKit
import CoreData



class CoreDataManager {
    
    public static func getMoviesOfSomeType(request: CoreDataManagerModel.GetMoviesOfSomeType.Request) -> CoreDataManagerModel.GetMoviesOfSomeType.Response {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let context = appDelegate.persistentContainer.viewContext
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "Movie")
        let predicate = NSPredicate.init(format: "type = %d", request.type.rawValue)
        fetchRequest.predicate = predicate
        
        do {
            let elements = try context.fetch(fetchRequest) as? [Movie]
            guard let movies = elements else {
                return CoreDataManagerModel.GetMoviesOfSomeType.Response.init(movies: [Movie](), error: self.getMovieFetchingErrorType(type: request.type))
            }
            return CoreDataManagerModel.GetMoviesOfSomeType.Response.init(movies: movies, error: nil)
        }catch{
            return CoreDataManagerModel.GetMoviesOfSomeType.Response.init(movies: [Movie](), error: self.getMovieFetchingErrorType(type: request.type))
        }
    }
    
    public static func getTVShowsOfSomeType(request: CoreDataManagerModel.GetTVShowsOfSomeType.Request) -> CoreDataManagerModel.GetTVShowsOfSomeType.Response {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let context = appDelegate.persistentContainer.viewContext
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "TVShow")
        let predicate = NSPredicate.init(format: "type = %d", request.type.rawValue)
        fetchRequest.predicate = predicate
        
        do {
            let elements = try context.fetch(fetchRequest) as? [TVShow]
            guard let tvshows = elements else {
                return CoreDataManagerModel.GetTVShowsOfSomeType.Response.init(tvshows: [TVShow](), error: self.getTVShowFetchingErrorType(type: request.type))
            }
            return CoreDataManagerModel.GetTVShowsOfSomeType.Response.init(tvshows: tvshows, error: nil)
        }catch{
            return CoreDataManagerModel.GetTVShowsOfSomeType.Response.init(tvshows: [TVShow](), error: self.getTVShowFetchingErrorType(type: request.type))
        }
    }
    
    public static func removeAllMoviesOfType(type: TypeOfMovies) throws {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let context = appDelegate.persistentContainer.viewContext
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "Movie")
        do {
            let elements = try context.fetch(fetchRequest) as? [Movie]
            guard let movies = elements else {
                throw ErrorType.LocalStorage.errorDeletingMovies
            }
            for movie in movies {
                if movie.type == type.rawValue {
                    context.delete(movie)
                }
            }
            try context.save()
        } catch {
            throw ErrorType.LocalStorage.errorDeletingMovies
        }
    }
    
    public static func removeAllTVShowsOfType(type: TypeOfTVShows) throws {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let context = appDelegate.persistentContainer.viewContext
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "TVShow")
        do {
            let elements = try context.fetch(fetchRequest) as? [TVShow]
            guard let tvshows = elements else {
                throw ErrorType.LocalStorage.errorDeletingTVShows
            }
            for tvshow in tvshows {
                if tvshow.type == type.rawValue {
                    context.delete(tvshow)
                }
            }
            try context.save()
        } catch {
            throw ErrorType.LocalStorage.errorDeletingTVShows
        }
    }
    
}

extension CoreDataManager {
    
    private static func getMovieFetchingErrorType(type: TypeOfMovies) -> ErrorType.LocalStorage {
        var error: ErrorType.LocalStorage!
        switch type {
        case .popular:
            error = ErrorType.LocalStorage.errorFetchingPopularMoviesFromCoreData
        case .topRated:
            error = ErrorType.LocalStorage.errorFetchingTopRatedMoviesFromCoreData
        case .upcoming:
            error = ErrorType.LocalStorage.errorFetchingUpcomingMoviesFromCoreData
        case .searched:
            error = ErrorType.LocalStorage.errorFetchingSearchingMoviesFromCoreData
        }
        return error
    }
    
    private static func getTVShowFetchingErrorType(type: TypeOfTVShows) -> ErrorType.LocalStorage {
        var error: ErrorType.LocalStorage!
        switch type {
        case .popular:
            error = ErrorType.LocalStorage.errorFetchingPopularTVShowsFromCoreData
        case .topRated:
            error = ErrorType.LocalStorage.errorFetchingTopRatedTVShowsFromCoreData
        case .searched:
            error = ErrorType.LocalStorage.errorFetchingSearchingTVShowsFromCoreData
        }
        return error
    }
    
}
