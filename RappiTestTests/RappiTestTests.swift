//
//  RappiTestTests.swift
//  RappiTestTests
//
//  Created by Israel Gtz on 8/8/19.
//  Copyright © 2019 Rohkeus Systems. All rights reserved.
//

import XCTest
@testable import RappiTest

class RappiTestTests: XCTestCase {
    
    func testNetworkManagerGetPopularMovies() {
        NetworkManager.shared.doRequest(type: .getPopularMovies, parameters: nil).done { (response) in
            XCTAssert((response as Any) is PopularMoviesResponse) //pass
            XCTAssertFalse((response as Any) is TopRatedMoviesResponse)
            XCTAssertFalse((response as Any) is UpcomingMoviesResponse)
            XCTAssertFalse((response as Any) is SearchingMoviesResponse)
            XCTAssertFalse((response as Any) is PopularTVShowResponse)
            XCTAssertFalse((response as Any) is TopRatedTVShowResponse)
            XCTAssertFalse((response as Any) is SearchingTVShowsResponse)
            XCTAssertFalse((response as Any) is ConfigurationRequestServerResponse)
        }.catch { (error) in
            print(error)
        }
    }
    
    func testNetworkManagerGetTopRatedMovies() {
        NetworkManager.shared.doRequest(type: .getTopRatedMovies, parameters: nil).done { (response) in
            XCTAssert((response as Any) is TopRatedMoviesResponse) //pass
            XCTAssertFalse((response as Any) is PopularMoviesResponse)
            XCTAssertFalse((response as Any) is UpcomingMoviesResponse)
            XCTAssertFalse((response as Any) is SearchingMoviesResponse)
            XCTAssertFalse((response as Any) is PopularTVShowResponse)
            XCTAssertFalse((response as Any) is TopRatedTVShowResponse)
            XCTAssertFalse((response as Any) is SearchingTVShowsResponse)
            XCTAssertFalse((response as Any) is ConfigurationRequestServerResponse)

            }.catch { (error) in
                print(error)
        }
    }
    
    func testNetworkManagerGetUpcomingMovies() {
        NetworkManager.shared.doRequest(type: .getUpcomingMovies, parameters: nil).done { (response) in
            XCTAssert((response as Any) is UpcomingMoviesResponse) //pass
            XCTAssertFalse((response as Any) is PopularMoviesResponse)
            XCTAssertFalse((response as Any) is TopRatedMoviesResponse)
            XCTAssertFalse((response as Any) is SearchingMoviesResponse)
            XCTAssertFalse((response as Any) is PopularTVShowResponse)
            XCTAssertFalse((response as Any) is TopRatedTVShowResponse)
            XCTAssertFalse((response as Any) is SearchingTVShowsResponse)
            XCTAssertFalse((response as Any) is ConfigurationRequestServerResponse)

            }.catch { (error) in
                print(error)
        }
    }
    
    func testNetworkManagerGetSearchingMovies() {
        NetworkManager.shared.doRequest(type: .getSearchingMovies, parameters: nil).done { (response) in
            XCTAssert((response as Any) is SearchingMoviesResponse) //pass
            XCTAssertFalse((response as Any) is PopularMoviesResponse)
            XCTAssertFalse((response as Any) is TopRatedMoviesResponse)
            XCTAssertFalse((response as Any) is UpcomingMoviesResponse)
            XCTAssertFalse((response as Any) is PopularTVShowResponse)
            XCTAssertFalse((response as Any) is TopRatedTVShowResponse)
            XCTAssertFalse((response as Any) is SearchingTVShowsResponse)
            XCTAssertFalse((response as Any) is ConfigurationRequestServerResponse)
            }.catch { (error) in
                print(error)
        }
    }
    
    func testNetworkManagerGetPopularTVShows() {
        NetworkManager.shared.doRequest(type: .getPopularTVShows, parameters: nil).done { (response) in
            XCTAssert((response as Any) is PopularTVShowResponse) //pass
            XCTAssertFalse((response as Any) is PopularMoviesResponse)
            XCTAssertFalse((response as Any) is TopRatedMoviesResponse)
            XCTAssertFalse((response as Any) is UpcomingMoviesResponse)
            XCTAssertFalse((response as Any) is SearchingMoviesResponse)
            XCTAssertFalse((response as Any) is TopRatedTVShowResponse)
            XCTAssertFalse((response as Any) is SearchingTVShowsResponse)
            XCTAssertFalse((response as Any) is ConfigurationRequestServerResponse)
            }.catch { (error) in
                print(error)
        }
    }
    
    func testNetworkManagerGetTopRatedTVShows() {
        NetworkManager.shared.doRequest(type: .getTopRatedTVShows, parameters: nil).done { (response) in
            XCTAssert((response as Any) is TopRatedTVShowResponse) //pass
            XCTAssertFalse((response as Any) is PopularMoviesResponse)
            XCTAssertFalse((response as Any) is TopRatedMoviesResponse)
            XCTAssertFalse((response as Any) is UpcomingMoviesResponse)
            XCTAssertFalse((response as Any) is SearchingMoviesResponse)
            XCTAssertFalse((response as Any) is PopularTVShowResponse)
            XCTAssertFalse((response as Any) is SearchingTVShowsResponse)
            XCTAssertFalse((response as Any) is ConfigurationRequestServerResponse)
            }.catch { (error) in
                print(error)
        }
    }
    
    func testNetworkManagerGetSearchingTVShows() {
        NetworkManager.shared.doRequest(type: .getSearchingTVShows, parameters: nil).done { (response) in
            XCTAssert((response as Any) is SearchingTVShowsResponse) //pass
            XCTAssertFalse((response as Any) is PopularMoviesResponse)
            XCTAssertFalse((response as Any) is TopRatedMoviesResponse)
            XCTAssertFalse((response as Any) is UpcomingMoviesResponse)
            XCTAssertFalse((response as Any) is SearchingMoviesResponse)
            XCTAssertFalse((response as Any) is PopularTVShowResponse)
            XCTAssertFalse((response as Any) is TopRatedTVShowResponse)
            XCTAssertFalse((response as Any) is ConfigurationRequestServerResponse)
            }.catch { (error) in
                print(error)
        }
    }
    
    func testNetworkManagerGetConfiguration() {
        NetworkManager.shared.doRequest(type: .getConfigurationPaths, parameters: nil).done { (response) in
            XCTAssert((response as Any) is ConfigurationRequestServerResponse) //pass
            XCTAssertFalse((response as Any) is PopularMoviesResponse)
            XCTAssertFalse((response as Any) is TopRatedMoviesResponse)
            XCTAssertFalse((response as Any) is UpcomingMoviesResponse)
            XCTAssertFalse((response as Any) is SearchingMoviesResponse)
            XCTAssertFalse((response as Any) is PopularTVShowResponse)
            XCTAssertFalse((response as Any) is TopRatedTVShowResponse)
            XCTAssertFalse((response as Any) is SearchingTVShowsResponse)
            }.catch { (error) in
                print(error)
        }
    }

}
